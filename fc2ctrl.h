#ifndef fc2ctrl_h
#define fc2ctrl_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

uint32_t* iomap(const char* str, uint32_t size);
void iounmap(uint32_t* p, uint32_t size);


#endif
