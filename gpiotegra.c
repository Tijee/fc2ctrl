#include "fc2ctrl.h"

#define gpio_jtag_xfer_imp
#include "gpio.h"

#include <pthread.h>
#include <sched.h>


#define SYSFS_GPIO_DIR "/sys/class/gpio"


#ifdef PLATFORM_TX1

/*
---        sch     -------------   pinmux ---    pinctrl -
WAKEUP  GPIO10_WIFI_WAKE_AP  B20   GPIO3_PI.01   65
MCUPDN  GPIO12_BT_EN         B21   GPIO3_PI.02   66
MCUPROG GPIO13_BT_WAKE_AP    B22   GPIO3_PH.06   62
FPGATMS GPIO16_MDM_WAKE_AP   A10   GPIO3_PX.00   184
FPGATCK GPIO17_MDM2AP_READY  B9    GPIO3_PK.04   84
FPGATDO GPIO18_MDM_COLDBOOT  B10   GPIO3_PK.06   86
FPGATDI GPIO19_AUD_RST       F2    GPIO3_PBB.03  219
MDOC    USB0_EN_OC           A17   GPIO3_PCC.04  228
*/

#define PIN_WAKEUP   65
#define PIN_MCUPDN   66
#define PIN_MCUPROG  63
#define PIN_FPGATMS  184
#define PIN_FPGATCK  84
#define PIN_FPGATDO  86
#define PIN_FPGATDI  219
#define PIN_MDOC     228

#endif

static volatile uint32_t* gpiobase = NULL;
bool gpio_init()
{
    if(!gpiobase)gpiobase = iomap("gpio@6000d000", 4096);
    return gpiobase != NULL;
}

void gpio_mode(uint8_t idx, GPIOMODE mode)
{
    if(!gpiobase)return;
    
    int fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
    if(fd < 0) return;
    
    char str[64];
    int len = snprintf(str, sizeof(str), "%d", idx);
    write(fd, str, len);
    close(fd);
    
    sprintf(str, SYSFS_GPIO_DIR "/gpio%d/direction", idx);
    fd = open(str, O_WRONLY);
    if(fd < 0)return;
    
    if(mode == GPIOMODE_INPUT){
        write(fd, "in", 3);
    } else {
        write(fd, "out", 4);
    }
    close(fd);
}

#define iread(idx) (gpiobase[ (0x40 * (idx>>5)) + 0xc + ((idx>>3)&3) ] >> (idx&7)) & 1
bool gpio_read(uint8_t idx)
{
    if(!gpiobase)return 0;
    return iread(idx);
}

#define iset(idx) gpiobase[ (0x40 * (idx>>5)) + 0x28 + ((idx>>3)&3) ] = 0x101 << (idx&7)
void gpio_set(uint8_t idx)
{
    if(!gpiobase)return;
    iset(idx);
}

#define iclr(idx) gpiobase[ (0x40 * (idx>>5)) + 0x28 + ((idx>>3)&3) ] = 0x100 << (idx&7)
void gpio_clr(uint8_t idx)
{
    if(!gpiobase)return;
    iclr(idx);
}

bool gpio_jtag_init()
{
    if(!gpio_init())return 0;
    
    gpio_jtag_write(0, 1, 0);
    
    gpio_mode(PIN_FPGATMS, GPIOMODE_OUTPUT);
    gpio_mode(PIN_FPGATCK, GPIOMODE_OUTPUT);
    gpio_mode(PIN_FPGATDI, GPIOMODE_OUTPUT);
    gpio_mode(PIN_FPGATDO, GPIOMODE_INPUT);
    
    gpio_jtag_write(0, 1, 0);
    return 1;
}

void gpio_jtag_quit(void)
{
    gpio_mode(PIN_FPGATMS, GPIOMODE_INPUT);
    gpio_mode(PIN_FPGATCK, GPIOMODE_INPUT);
    gpio_mode(PIN_FPGATDI, GPIOMODE_INPUT);
}

bool gpio_jtag_read(void)
{
    return iread(PIN_FPGATDO);
}

void gpio_jtag_write(bool tck, bool tms, bool tdi)
{
    if(tms) iset(PIN_FPGATMS); else iclr(PIN_FPGATMS);
    if(tdi) iset(PIN_FPGATDI); else iclr(PIN_FPGATDI);
    if(tck) iset(PIN_FPGATCK); else iclr(PIN_FPGATCK);
    
    for(int i=0; i<3; i++){
        gpiobase[0x28] = i;
    }
}


int readgmode(int h, char* buf)
{
    int sz = 0;
    for(int i = 0; i < 10; i++){
        usleep(500000);
        int n = read(h, &buf[sz], 32-sz-1);
        if( n > 0) {
            sz += n;
            if(sz >= 9)break;
        }
    }
    buf[2] = 0;
    return strtoul(buf, NULL, 16);
}

int fpgamode(int mode)
{
    if(mode == 4){
        if(!gpio_init()){
            fprintf(stderr, "gpio_init failed\n");
            return 1;
        }
        
        gpio_mode(PIN_MDOC, GPIOMODE_OUTPUT);
        gpio_clr(PIN_MDOC);
        gpio_mode(PIN_MDOC, GPIOMODE_INPUT);
        return 0;
    }
    
    char buf[32];
    int flags = -1;
    int h = open("/dev/ttyACM0", O_RDWR);
    if(h > 0) flags = fcntl(h, F_GETFL, 0);
    if(h <= 0 || flags == -1){
        fprintf(stderr, "Error opening ttyACM0 as PM\n");
        return 1;
    }
    fcntl(h, F_SETFL, flags | O_NONBLOCK);
    write(h, "\n", 1);

    int gmode = readgmode(h, buf);
    printf("GMODE=%02x\n", gmode);
    
    if(mode == 1 || mode == 3){
        gmode &= ~0x40;
        sprintf(buf, "00%02X\n", gmode);
        write(h, buf, 5);
        
        readgmode(h, buf);
        printf("GMODE=%02x\n", gmode);
        if(mode == 3) return 0;
    }
    
    if(mode == 1 || mode == 2){
        gmode |= 0x40;
        sprintf(buf, "00%02X\n", gmode);
        write(h, buf, 5);

        gmode = readgmode(h, buf);
        printf("GMODE=%02x\n", gmode);
        return 0;
    }
    
    return 1;        
}
