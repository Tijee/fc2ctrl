#!/usr/bin/python

import io, sys, socket, time, struct
from ctypes import *


class JTAG(object):
    RESET = (5, 0x1f, 0)
    
    TLR = 0
    RTI = 1
    SDS = 2
    CDR = 3
    SDR = 4
    DR1 = 5
    PDR = 6
    DR2 = 7
    UDR = 8
    SIS = 9
    CIR = 10
    SIR = 11
    IR1 = 12
    PIR = 13
    IR2 = 14
    UIR = 15
    STATE = ('TLR', 'RTI', 'SDS', 'CDR', 'SDR', 'DR1', 'PDR', 'DR2', 'UDR', 'SIS', 'CIR', 'SIR', 'IR1', 'PIR', 'IR2', 'UIR')
    MACHINE = ( 
        (RTI, TLR), 
        (RTI, SDS),
        (CDR, SIS),
        (SDR, DR1),
        (SDR, DR1),
        (PDR, UDR),
        (PDR, DR2),
        (SDR, UDR),
        (RTI, SDS),
        (CIR, TLR),
        (SIR, IR1),
        (SIR, IR1),
        (PIR, UIR),
        (PIR, IR2),
        (SIR, UIR),
        (RTI, SDS) )
    
    def __init__(self):
        self.state = JTAG.TLR
        self.ir = 0
        self.drs = []
        self.onxvcidle = None
        
    def onsetdr(self):
        #print self.ir, self.drs
        if len(self.drs) == 3 and self.drs[0][1] == 76546432:
            print self.ir, self.drs

    def tapsim(self, bc, tms, tdi, tdo):
        idr = 0
        odr = 0
        iir = 0
        oir = 0
        didx = 0
        iidx = 0
        
        mirs = (2, 3, 0x22, 0x23)
        
        #print bc, map(hex, tms), map(hex, tdi), map(hex, tdo)
        for n in range(len(tms)):
            if bc == 0: break
            
            cms = tms[n]
            cdi = tdi[n]
            cdo = tdo[n]
            if cms == 0 and self.state in (JTAG.RTI, JTAG.SDR, JTAG.PDR, JTAG.SIR, JTAG.PIR):
                if bc > 8: bc -= 8
                else: bc = 0
                if self.state == JTAG.SDR:
                    idr |= cdi<<didx
                    odr |= cdo<<didx
                    didx += 8
                    if bc == 0 and iir in mirs:
                        self.drs.append( (didx, idr) )
                    #if bc == 0: print 'DR=%x %x' % (idr, odr)
                if self.state == JTAG.SIR:
                    iir |= cdi<<iidx
                    oir |= cdo<<iidx
                    iidx += 8
                    if bc == 0: 
                        #print 'IR=%x %x' % (iir, oir)
                        if len(self.drs) > 0: self.onsetdr()
                        self.drs = []
                        self.ir = iir
                continue
            
            for c in range(8):
                if bc == 0: break
                bc -= 1
                bms = (cms>>c)&1
                bdi = (cdi>>c)&1
                bdo = (cdo>>c)&1
                if self.state == JTAG.CDR:
                    idr = 0
                    odr = 0
                    didx = 0
                if self.state == JTAG.CIR: 
                    iir = 0
                    oir = 0
                    iidx = 0
                if self.state == JTAG.SDR:
                    idr |= bdi<<didx
                    odr |= bdo<<didx
                    didx += 1
                    if (bms or bc == 0)  and iir in mirs:
                        #print 'DR=%x %x' % (idr, odr)
                        self.drs.append( (didx, idr) )
                if self.state == JTAG.SIR:
                    iir |= bdi<<iidx
                    oir |= bdo<<iidx
                    iidx += 1
                    if bms or bc == 0: 
                        #print 'IR=%x %x' % (iir, oir)
                        if len(self.drs) > 0: self.onsetdr()
                        self.ir = iir
                        self.drs = []
                    
                self.state = JTAG.MACHINE[self.state][bms]
                
        #print JTAG.STATE[self.state], bc, cms, cdi, cdo
        

        
    # data is [ (bcnt, tms, tdi), (bcnt, tms, tdi), ... ]
    # return  [ tdo, tdo, ... ]
    def run(self, data, cb=None):
        tms = bytearray()
        tdi = bytearray()
        tdo = bytearray()
        
        bc = 0
        btms = 0
        btdi = 0
        n = 0
        for d in data:
            if isinstance(d[2], bytearray):
                if bc>0 or len(tms)>0:
                    if bc>0:
                        tms.append( btms )
                        tdi.append( btdi )
                    bc += len(tms)*8
                    o = self.xfer(bc, tms, tdi)
                    tdo.extend(o)
                tms = bytearray(len(d[2]))
                self.xfer(d[0], tms, d[2])
                if cb: cb(n, 0)
                tms = bytearray()
                tdi = bytearray()
                bc = 0
                btms = 0
                btdi = 0
                
            else:
                idx = 0
                while idx < d[0]:
                    bw = min(d[0]-idx, 8-bc)
                    btms |= ((d[1]>>idx) & ((1<<bw)-1)) << (bc%8)
                    btdi |= ((d[2]>>idx) & ((1<<bw)-1)) << (bc%8)
                    bc += bw
                    idx += bw
                    if bc == 8:
                        tms.append( btms )
                        tdi.append( btdi )
                        if len(tms) == 1024:
                            o = self.xfer(len(tms)*8, tms, tdi)
                            #self.tapsim(len(tms)*8, tms, tdi, o)
                            tdo.extend(o)
                            if cb: cb(n, idx)
                            tms = bytearray()
                            tdi = bytearray()
                        bc = 0
                        btms = 0
                        btdi = 0
            n += 1

        if bc>0:
            tms.append( btms )
            tdi.append( btdi )
        bc += len(tms)*8
        if bc>0: 
            o = self.xfer(bc, tms, tdi)
            #self.tapsim(bc, tms, tdi, o)
            tdo.extend(o)
        
        ret = []
        bc = 0
        n = 0
        for d in data:
            if isinstance(d[2], bytearray): break
            
            idx = 0
            v = 0
            while idx < d[0]:
                bw = min(d[0] - idx, 8 - bc)
                v |= ((tdo[n]>>bc)&((1<<bw)-1)) << idx
                bc += bw
                idx += bw
                if bc == 8:
                    n += 1
                    bc = 0
            ret.append(v)
        return ret

        
        
    def xvclog(self, msg):
        print '[%s] %s' % (self.csaddr[0], msg)
    
    def xvcrecv(self, n):
        msg = bytearray()
        while n > 0:
            m = self.cs.recv(n)
            msg.extend(m)
            n -= len(m)
        return msg
    
    def xvcclient(self):
        # see UG908 page 43
        while True:
            msg = self.xvcrecv(4)
            if len(msg) == 0: break
            if msg == 'geti': # getinfo
                self.xvcrecv(4)
                self.cs.send('xvcServer_v1.0:4096\n')
                continue
            if msg == 'sett': # setttck
                msg = self.xvcrecv(7)
                self.cs.send(msg[3:])
                continue
            if msg == 'shif': # shift
                bc = struct.unpack('<xxI', self.xvcrecv(6))[0]
                bw = (bc+7)/8
                #print 'shift %d' % bc
                tms = self.xvcrecv(bw)
                tdi = self.xvcrecv(bw)
                if (tms[0]&0x1f) == 0x1f and self.onxvcidle:
                    self.onxvcidle()
                tdo = self.xfer(bc, tms, tdi)
                self.cs.send(tdo)
                #self.tapsim(bc, tms, tdi, tdo)
                continue
            self.xvclog('invalid XVC cmd "%s"' % msg)
            break
        
    def xvc(self):
        def getlocalip():
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("192.168.1.1", 80))
            ip = s.getsockname()[0]
            s.close()
            return ip
            
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('0.0.0.0', 6666))
        s.listen(1)
        print 'open_hw_target -xvc_url %s:6666' % getlocalip()
        while True:
            (self.cs, self.csaddr) = s.accept()
            self.xvclog('connect')
            try:
                self.xvcclient()
            except:
                import traceback
                traceback.print_exc()
                
            self.xvclog('disconnect')
            del self.cs
     

class JTAG_XVC(JTAG):
    def __init__(self, host, port):
        super(JTAG_XVC, self).__init__()
        self.fd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.fd.connect((host, port))
        self.fd.send(b'getinfo\n')
        if 'xvcServer' not in self.fd.recv(100).decode():
            raise ValueError("not a xvcServer")

    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.fd
            
    # bc: bits count
    # tms, tdi: bytearray
    def xfer(self, bc, tms, tdi):
        msg = bytearray(b'shift\n')
        msg.append(bc&0xff)
        msg.append((bc>>8)&0xff)
        msg.append((bc>>16)&0xff)
        msg.append((bc>>24)&0xff)
        msg.extend(tms)
        msg.extend(tdi)
        self.fd.send(msg)
        t = self.fd.recv(len(tms))
        if len(t) != len(tms):
            raise ValueError("recv len(tdo) != len(tms)")
        return bytearray(t)

        
class FC2GPIO:
    def __init__(self):
        ext = cdll.LoadLibrary("./fc2gpio.so")
        ret = ext.gpio_init()
        if ret != 1: raise ValueError("gpio_init return %d" % ret)
        self.INPUT = 0
        self.OUTPUT = 1
        self.mode = ext.gpio_mode
        self.read = ext.gpio_read
        self.set = ext.gpio_set
        self.clr = ext.gpio_clr
        self.jtag_init = ext.gpio_jtag_init
        self.jtag_quit = ext.gpio_jtag_quit
        self.jtag_xfer = ext.gpio_jtag_xfer
        self.jtag_xfer.argtypes = [ c_uint, c_void_p, c_void_p, c_char_p ]
        
        
class JTAG_LOCAL(JTAG):
    def __init__(self):
        super(JTAG_LOCAL, self).__init__()
        self.gpio = FC2GPIO()
    
    def __enter__(self):
        self.gpio.jtag_init()
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.gpio.jtag_quit()
        
    def xfer(self, bc, tms, tdi):
        #print 'xfer %s %s %s' % (bc, list(tms), list(tdi))
        tdo = create_string_buffer(len(tms))
        type = c_char * len(tms)
        self.gpio.jtag_xfer(bc, type.from_buffer(tms), type.from_buffer(tdi), tdo)
        return bytearray(tdo)

def s2b(s, v1='1'):
    v = 0
    for c in s: 
        v = (v<<1) | (1 if c == v1 else 0)
    return v
        
class CHIP:
    def __init__(self, fname, jtag):
        self.jtag = jtag
        with open(fname, 'r') as f:
            state = ''
            msg = ''
            for line in f:
                n = line.rfind('--')
                if n >= 0: line = line[:n]
                line = line.replace('\t', ' ').replace('  ', ' ').replace('  ', ' ').strip()
                ts = line.split(' ')
                if len(ts) == 0: continue
                if ts[0] == 'entity':
                    self.entity = ts[1]
                    continue
                if state == '':
                    if ts[0] == 'const' and ts[2] == 'PIN_MAP_STRING:=': 
                        state = 'PIN_MAP_STRING'
                        continue
                    if ts[0] == 'attribute' and ts[1] == 'INSTRUCTION_LENGTH':
                        self.INSTRUCTION_LENGTH = int(ts[-1][:-1])
                        continue
                    states = ('INSTRUCTION_OPCODE', 'IDCODE_REGISTER', 'BOUNDARY_REGISTER')
                    if ts[0] == 'attribute' and ts[1] in states:
                        state = ts[1]
                    msg = ''
                    continue
                if state != '':
                    a = line.find('"')
                    b = line.rfind('\"')
                    if a>=0 and b>a: msg += line[a+1:b]
                    
                if line.endswith(';'):
                    if state == 'INSTRUCTION_OPCODE': self.setopcode(msg)
                    if state == 'IDCODE_REGISTER': self.setidcode(msg)
                    if state == 'BOUNDARY_REGISTER': self.setboundary(msg)
                    state = ''

    def __enter__(self):
        self.jtag.__enter__()
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.jtag.__exit__(exc_type, exc_val, exc_tb)
                    
    def setopcode(self, msg):
        self.OPCODE = {}
        for v in msg.split(','):
            n,v = v.split(' ')
            # last TMS=1 to exit shift-IR
            v = (len(v)-2, 1<<(len(v)-3), s2b(v[1:-1]))  
            self.OPCODE[n] = v

    def setidcode(self, msg):
        self.IDCODE = s2b(msg)
        self.IDMASK = s2b(msg, 'X') ^ 0xffffffff

    # see "The Boundary-Scan Handbook" https://books.google.com/books?id=V_PpCgAAQBAJ
    def setboundary(self, msg):
        self.REGS = {}
        self.REGSIZE = 0
        msg = msg.replace('(', '').replace('),', '$').replace(', ', ' ')
        for v in msg.split('$'):
            ts = v.strip().split(' ')
            self.REGSIZE += 1
            if ts[2] == '*': continue
            ts[3] = ts[3].lower()
            z = [-1, -1, -1]
            if ts[2] in self.REGS: z = self.REGS[ ts[2] ]
            if ts[3] == 'output3':
                z[1] = int(ts[0])
                z[2] = int(ts[5])
            if ts[3] == 'output2':
                z[1] = int(ts[0])
            if ts[3] in ('input', 'observe_only'):
                z[0] = int(ts[0])
            self.REGS[ ts[2] ] = z
        
    def getidcode(self):
        idcode = self.jtag.run( [JTAG.RESET, (4, 0x02, 0), (32, 0, 0)] )[2]
        return idcode & self.IDMASK
        
    def checkidcode(self):
        idcode = self.getidcode()
        if idcode != self.IDCODE:
            print 'ID=%x, not a %s' % (idcode, self.entity)
            sys.exit(1)

        print 'CHIP =', self.entity

    def getxadc(self):
        result = self.jtag.run( [JTAG.RESET, (5, 0x06, 0), self.OPCODE['XADC_DRP'], 
            (4, 0x3, 0), (32, 1<<31, 0x04000000), 
            (4, 0x3, 0), (32, 1<<31, 0x04010000),
            (4, 0x3, 0), (32, 1<<31, 0x04020000),
            (4, 0x3, 0), (32, 0, 0)
            ] )
        return ( (result[6]*503.98/65536)-273.15, result[8]*3.0/65536, result[10]*3.0/65536)

    def showinfo(self):
        for i in range(10):
            print 'TEMP = %.1f' % self.getxadc()[0]
            time.sleep(1)
            
    def test(self):
        sample = self.jtag.run( [JTAG.RESET, (5, 0x06, 0), self.OPCODE['SAMPLE'], (4, 0x3, 0), (self.REGSIZE, 0, 0)] )[4]
        
        def getv(pin):
            v = []
            for i in self.REGS[pin]:
                if i<0: v.append('-')
                else: v.append( (sample >> i) & 1 )
            return v
            
        pins = ('IO_M16', 'DONE_P6')
        for pin in pins:
            print '%s = %s' % (pin, getv(pin))
            
        #sample ^= 1 << self.REGS['IO_M16'][2]
        #self.jtag.run( [JTAG.RESET, (5, 0x6, 0), self.OPCODE['INTEST_RSVD'], (4, 0x3, 0),  (self.REGSIZE, 1<<(self.REGSIZE-1), sample), (4, 1, 0) ] )
        
        #sample ^= 1 << self.REGS['DONE_P6'][2]
        #sample ^= 1 << self.REGS['DONE_P6'][1]
        #self.jtag.run( [JTAG.RESET, 
        #    (5, 0x06, 0), self.OPCODE['PRELOAD'], (4, 0x3, 0), (self.REGSIZE, 1<<(self.REGSIZE-1), sample),
        #    (5, 0x7, 0), self.OPCODE['EXTEST'], (4, 0x3, 0),  (self.REGSIZE, 1<<(self.REGSIZE-1), sample), (4, 1, 0) ] )
        #self.jtag.run( [JTAG.RESET] )
        
    def download(self, fname, callback=None):
        # JTAG see UG470 page 178
        self.jtag.run( [JTAG.RESET, (5, 0x06, 0), self.OPCODE['JPROGRAM']]  )

        # stream see UG470 page 104   
        bitstream = [JTAG.RESET, (10000, 0, 0), (5, 0x06, 0), self.OPCODE['CFG_IN'], (4, 3, 0)]
        
        rtable = []
        for v in range(256):
            t = 0
            for n in range(8):
                t = t << 1
                if v&(1<<n): t |= 1
            rtable.append(t)
        
        def badd(bs):
            for i in range(len(bs)):
                bs[i] = rtable[ bs[i] ]
            bitstream.append( (len(bs)*8, 0, bs) )
        
        global oldv
        oldv = -1
        def progress(n, k):
            global oldv
            v = n*100/len(bitstream)
            if v == oldv: return
            oldv = v
            if callback:
                callback(v)
            else:
                sys.stdout.write('\r%02d%%' % v)
                sys.stdout.flush()
        
        with open(fname, 'rb') as f:
            bs = bytearray(f.read(256))
            hdr = 0
            for i in range(len(bs)): 
                if bs[i] == 0xaa and bs[i+1] == 0x99 and bs[i+2] == 0x55 and bs[i+3] == 0x66:
                    hdr = i - 12*4
            if hdr == 0:
                print 'can not find bitstream header in %s' % fname
                sys.exit(1)
            
            badd( bs[hdr:] )
            while True:
                bs = bytearray(f.read(256))
                if len(bs) == 0: break
                badd( bs )
            bs = bitstream[-1][2]
            bitstream[-1] = ( (len(bs)-1)*8, 0, bs[:-1] )
            bitstream.append( (8, 1<<7, bs[-1]) )  # last bit need TMS
        
        # check INIT flag
        for i in range(100):
            flag = self.jtag.run( [JTAG.RESET, (5, 0x06, 0), self.OPCODE['BYPASS']]  )[2]
            if (flag & 0x10) != 0: break
            
        # download bitstream
        bitstream.extend([(6, 0x5, 0), self.OPCODE['JSTART'], (2400, 1, 0)])
        self.jtag.run(bitstream, progress)
        
        # check DONE flag
        flag = self.jtag.run( [JTAG.RESET, (5, 0x06, 0), self.OPCODE['BYPASS']]  )[2]
        print "\rDONE =", flag>>5
        return flag>>5
    

if __name__ == "__main__":
    with CHIP('xc7k160t_fbg484.bsd', JTAG_LOCAL()) as chip:
        chip.checkidcode()
        
        if sys.argv[-1] == '-x':
            chip.jtag.xvc()

        elif sys.argv[-1] == '-t':
            chip.test()
            
        elif sys.argv[-1].endswith('.bit'):
            chip.download(sys.argv[-1])
            
        elif sys.argv[-1].endswith('.tst'):
            pass

        else:
            chip.showinfo()

