#!/usr/bin/python

import sys, io, os, time, struct

class WCHISP:
    '''
    wchprog https://github.com/juliuswwj/wchprog
    A python script that programs firmware of CH55x chips through USB link.

    needs: pyusb 1.0 https://walac.github.io/pyusb/

    under ubuntu:

    sudo apt-get install python-pip sudo pip install pyusb

    WCH forum http://www.wch.cn/bbs/thread-65023-1.html

    MIT License
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    Copyright
    (C) 2017 juliuswwj@gmail.com
    '''
    def __init__(self):
        import usb.core
        import usb.util
        
        # find our device

        dev = usb.core.find(idVendor=0x4348, idProduct=0x55e0)
        if dev is None:
            raise ValueError('Device not found')
        
        dev.set_configuration()
        cfg = dev.get_active_configuration()
        intf = cfg[(0,0)]
        
        self.epout = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT)
        self.epin = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_IN)

    def cmd(self, msg):
        self.writeb(msg)
        b = self.readb(64)
        if len(b) == 2:
            return struct.unpack('<H', b)[0]
        return b

    def xcmd(self, msg, exp):
        #xmsg = map(lambda x: hex(ord(x))[2:], msg)
        #print ' '.join(xmsg)
        #return 0

        ret = self.cmd(msg)
        if ret != exp: 
            xmsg = map(lambda x: hex(ord(x)), msg[0:4])
            raise Exception('cmd[%s] return %d != %d' % (','.join(xmsg), ret, exp)) 

    def info(self):
        v = self.cmd('\xa2\x13USB DBG CH559 & ISP' + '\0'*42)
        self.cmd('\xbb\x00')
        return v

    def readb(self, size):
        return self.epin.read(size)

    def writeb(self, b):
        self.epout.write(b)

    def program(self, hexfile):
        def readhex():
            lno = 0
            mem = []
            with open(hexfile, 'r') as f:
                for line in f:
                    lno += 1
                    line = line.strip()
                    if len(line)<6 or line[0] != ':': continue
                    if line[7:9] == '01': break
                    if line[7:9] != '00': 
                        raise ValueException('unknown data type @ %s:%d' % (hexfile, lno))
                    n = int(line[1:3], 16)
                    addr = int(line[3:7], 16)
                    if n + addr > len(mem): 
                        mem.extend([255] * (n+addr-len(mem)))
                    i = 9
                    while n > 0:
                        mem[addr] = int(line[i:i+2], 16)
                        i += 2
                        addr += 1
                        n -= 1
            return mem

        def wv(mode):
            if mode == '\xa7':
                print 'Verifying ',
            else:
                print 'Programming ',
            sys.stdout.flush()

            addr = 0
            while addr < len(mem):
                b = mode
                sz = len(mem) - addr
                if sz > 0x3c: sz = 0x3c
                b += struct.pack('<BH', sz, addr)
                for i in range(sz):
                    b += chr(mem[addr+i]^rand[i%4])
                self.xcmd(b, 0)
                addr += sz
                sys.stdout.write('#')
                sys.stdout.flush()
            print ''

        rand = (0x29, 0x52, 0x8C, 0x70)
        mem = readhex()
        if len(mem) < 256 or len(mem) > 16384: 
            raise "hexfile codesize %d not in (256, 16384)" % len(mem)

        b = '\xa6\x04' + struct.pack('BBBB', *rand)
        self.xcmd(b, 0)
        for page in range(0, 0x40, 4):
            b = '\xa9\x02\x00' + chr(page)
            self.xcmd(b, 0)

        wv('\xa8')

        self.cmd('\xb8\x02\xff\x4e') # Code_Protect, Boot_Load, No_Long_Reset, No_RST 
        self.cmd('\xb9\x00')

        wv('\xa7')
        self.writeb('\xa5\x02\x00\x00')
        

def genko():
    import gzip, subprocess
    
    with open('/sys/module/tegra_fuse/parameters/tegra_chip_id') as f:
        v = int(f.readline().strip())
        
    if v != 33:
        print 'current system is NOT TX1'
        sys.exit(1)
        
    with open('/proc/version') as f:
        v = f.readline().strip()

    KVERSION='4.4.38-tegra'
    if KVERSION not in v:
        print 'current system is NOT R28.1'
        sys.exit(1)


    SRCDIR = '/usr/src/kernel/kernel-4.4'
    CONFIG = SRCDIR + '/.config'
    if not os.path.isfile(CONFIG):
        os.chdir('/run')
        
        if os.system('curl -sSL https://developer.download.nvidia.com/embedded/L4T/r28_Release_v2.1/public_sources.tbz2 | tar xfvj -') != 0:
            print 'download r28_Release_v2.1 error'
            sys.exit(2)
        
        os.chdir('/usr/src')
        if os.system('tar xfvj /run/public_release/kernel_src.tbz2') != 0:
            print 'extract source error'
            sys.exit(3)
        
        os.system('rm -rf /run/public_release')
        os.chdir(SRCDIR)
        
        with open('.config', 'w') as fo:
            with gzip.open('/proc/config.gz', 'rb') as fi:
                for line in fi:
                    line = line.strip()
                    if 'CONFIG_SPI_SPIDEV' in line: line = 'CONFIG_SPI_SPIDEV=m'
                    if 'CONFIG_LOCALVERSION=' in line: line = 'CONFIG_LOCALVERSION="-tegra"'
                    if 'CONFIG_DEFAULT_HOSTNAME=' in line: line = 'CONFIG_DEFAULT_HOSTNAME="tijee"'
                    print >>fo, line

    SPIDEVKO = '/lib/modules/%s/kernel/drivers/spidev.ko' % KVERSION
    if not os.path.isfile(SPIDEVKO):
        os.chdir(SRCDIR)
        os.system('make prepare')
        os.system('make modules_prepare')
        os.system('make M=drivers/spi/')
        os.system('cp drivers/spi/spidev.ko %s' % SPIDEVKO)
        os.system('depmod')

def gendtb(printdts):
    # https://github.com/superna9999/pyfdt/raw/master/pyfdt/pyfdt.py
    try:
        from pyfdt import FdtFsParse
    except:
        print 'pyfdt is missing, please run following command'
        print 'wget https://github.com/superna9999/pyfdt/raw/master/pyfdt/pyfdt.py'
        sys.exit(2)
        
    from pyfdt import FdtFsParse, FdtBlobParse, FdtNode, FdtProperty, FdtPropertyBytes, FdtPropertyWords, FdtPropertyStrings

    dts = '/boot/tegra210-jetson-tx1-p2597-2180-a01-devkit.dtb'
    if len(sys.argv) == 3 and sys.argv[2].endswith('.dtb'):
        dts = sys.argv[2]
        
    with open(dts, 'rb') as f:
        fdt = FdtBlobParse(f).to_fdt()

    def add(node, key, value):
        n = node._find(key)
        if n is None: 
            n = len(node.subdata)
        else:
            node.pop(n)
            
        if isinstance(value, dict):
            subnode = FdtNode(key)
            subnode.set_parent_node(node)
            node.insert(n, subnode)
            for k,v in value.items():
                add(subnode, k, v)
                
        elif isinstance(value, list):
            if len(value) < 2:
                raise Exception("Invalid list for %s" % key)
            if value[0] == "words":
                words = [int(word, 16) for word in value[1:]]
                node.insert(0, FdtPropertyWords(key, words))
            elif value[0] == "bytes":
                bytez = [int(byte, 16) for byte in value[1:]]
                node.insert(0, FdtPropertyBytes(key, bytez))
            elif value[0] == "strings":
                node.insert(0, FdtPropertyStrings(key, \
                            [s for s in value[1:]]))
            else:
                raise Exception("Invalid list for %s" % key)
        elif value is None:
            node.insert(0, FdtProperty(key))
        else:
            raise Exception("Invalid value for %s" % key)

                
    # remove local sound
    fdt.get_rootnode().remove('sound')
    fdt.get_rootnode().remove('sound_ref')
    # remove some i2c
    fdt.get_rootnode().remove('i2c@7000c700')
    tm = time.ctime()
    add(fdt.get_rootnode(), 'nvidia,dtbbuildtime', ['strings', tm[4:10] + tm[19:24], tm[11:19]])

    pluginm_node = fdt.resolve_path('/plugin-manager')
    pluginm_node.remove('fragment-e3326@0')
    pluginm_node.remove('fragment-e3323@0')
    pluginm_node.remove('fragment-e3333@0')
    pluginm_node.remove('fragment-imx185@0')
    pluginm_node.remove('fragment-imx274@0')
    vii2c_node = fdt.resolve_path('/host1x/i2c@546c0000')
    vii2c_node.remove('tca9546@70');
    vii2c_node.remove('tca9548@77');
    #add vi port    
    vi_port0 = {
	'reg' : ['words', '0x0'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0xf7'],
	'phandle' : ['words', '0xf7'],
	'endpoint': {
	    'csi-port' : ['words', '0x0'],
	    'bus-width' : ['words', '0x4'],
	    'remote-endpoint' : ['words', '0x7e'],
	    'status' : ['strings', 'okay'],
	    'linux,phandle' : ['words', '0x9d'],
	    'phandle' : ['words', '0x9d'],
	    },
    };

    vi_port1 = {
	'reg' : ['words', '0x1'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x105'],
	'phandle' : ['words', '0x105'],
	'endpoint': {
	    'csi-port' : ['words', '0x2'],
	    'bus-width' : ['words', '0x2'],
	    'remote-endpoint' : ['words', '0x7f'],
	    'status' : ['strings', 'okay'],
	    'linux,phandle' : ['words', '0x9f'],
	    'phandle' : ['words', '0x9f'],
	    },
    };


    vi_node = fdt.resolve_path('/host1x/vi/ports')
    add(vi_node, 'port@0', vi_port0)
    add(vi_node, 'port@1', vi_port1)

    nvcsi_ch0 = {
	'reg' : ['words', '0x00000000'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x000000f9'],
	'phandle' : ['words', '0x000000f9'],
	'ports' : {
	    '#address-cells' : ['words', '0x00000001'],
	    '#size-cells' : ['words', '0x00000000'],
	    'port@0' : {
	        'reg' : ['words', '0x00000000'],
		'status' : ['strings', 'okay'],
		'linux,phandle' : ['words', '0x000000fa'],
		'phandle' : ['words', '0x000000fa'],
		'endpoint@0': {
			'csi-port' : ['words', '0x0'],
			'bus-width' : ['words', '0x4'],
			'remote-endpoint' : ['words', '0x131'],
			'status' : ['strings', 'okay'],
			'linux,phandle' : ['words', '0x00000093'],
			'phandle' : ['words', '0x00000093'],
		},
	    },
	    'port@1' :{
		'reg' : ['words', '0x00000001'],
		'status' : ['strings', 'okay'],
		'linux,phandle' : ['words', '0x000000fc'],
		'phandle' : ['words', '0x000000fc'],
		'endpoint@1' :{
			'remote-endpoint' : ['words', '0x0000009d'],
			'status' : ['strings', 'okay'],
			'linux,phandle' : ['words', '0x0000007e'],
			'phandle' : ['words', '0x0000007e'],
		},
	    },
	},
    };

    nvcsi_ch1 = {
	'reg' : ['words', '0x00000001'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x00000108'],
	'phandle' : ['words', '0x00000108'],
	'ports' : {
	    '#address-cells' : ['words', '0x00000001'],
	    '#size-cells' : ['words', '0x00000000'],
	    'port@0' : {
	        'reg' : ['words', '0x00000000'],
		'status' : ['strings', 'okay'],
		'linux,phandle' : ['words', '0x00000109'],
		'phandle' : ['words', '0x00000109'],
		'endpoint@2': {
			'csi-port' : ['words', '0x2'],
			'bus-width' : ['words', '0x2'],
			'remote-endpoint' : ['words', '0x132'],
			'status' : ['strings', 'okay'],
			'linux,phandle' : ['words', '0x00000094'],
			'phandle' : ['words', '0x00000094'],
		},
	    },
	    'port@1' :{
		'reg' : ['words', '0x00000001'],
		'status' : ['strings', 'okay'],
		'linux,phandle' : ['words', '0x0000010a'],
		'phandle' : ['words', '0x0000010a'],
		'endpoint@3' :{
			'remote-endpoint' : ['words', '0x0000009f'],
			'status' : ['strings', 'okay'],
			'linux,phandle' : ['words', '0x0000007f'],
			'phandle' : ['words', '0x0000007f'],
		},
	    },
	},
    };

    nvcsi_node = fdt.resolve_path('/host1x/nvcsi')
    add(nvcsi_node, 'channel@0', nvcsi_ch0)
    add(nvcsi_node, 'channel@1', nvcsi_ch1)

       
    cam_module0 = {
	'badge' : ['strings', 'imx185_bottom_360'],
	'position' : ['strings', 'bottom'],
	'orientation' : ['strings', '3'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x000000f3'],
	'phandle' : ['words', '0x000000f3'],
	'drivernode0': {
	    'pcl_id' : ['strings', 'v4l2_sensor'],
	    'devname' : ['strings', 'imx185 6-001a'],
	    'proc-device-tree' : ['strings', '/proc/device-tree/host1x/i2c@546c0000/imx185_a@1a'],
	    'status' : ['strings', 'okay'],
	    'linux,phandle' : ['words', '0x000000f4'],
	    'phandle' : ['words', '0x000000f4'],
	},
    };
    cam_module1 = {
	'badge' : ['strings', 'imx185_up_360'],
	'position' : ['strings', 'up'],
	'orientation' : ['strings', '3'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x000000f5'],
	'phandle' : ['words', '0x000000f5'],
	'drivernode1': {
	    'pcl_id' : ['strings', 'v4l2_sensor'],
	    'devname' : ['strings', 'imx185 6-001b'],
	    'proc-device-tree' : ['strings', '/proc/device-tree/host1x/i2c@546c0000/imx185_b@1b'],
	    'status' : ['strings', 'okay'],
	    'linux,phandle' : ['words', '0x000000f6'],
	    'phandle' : ['words', '0x000000f6'],
	},
    };
    tcm_node = fdt.resolve_path('/tegra-camera-platform/modules')
    tcm_node.remove('module1')
    tcm_node.remove('module2')
    tcm_node.remove('module3')
    tcm_node.remove('module4')
    tcm_node.remove('module5')
    add(tcm_node, 'module0', cam_module0)
    add(tcm_node, 'module1', cam_module1)

    imx185_a = {
	'compatible' : ['strings', 'nvidia,imx185'],
	'reg' : ['words', '0x0000001a'],
	'physical_w' : ['strings', '15.0'],
	'physical_h' : ['strings', '12.5'],
	'sensor_model' : ['strings', 'imx185'],
	'post_crop_frame_drop' : ['strings', '0'],
	'use_decibel_gain' : ['strings', 'true'],
	'delayed_gain' : ['strings', 'true'],
	'use_sensor_mode_id' : ['strings', 'true'],
	'clocks' : ['words', '0x00000041', '0x00000117'],
	'clock-names' : ['strings', 'clk_out_3'],
	'clock-frequency' : ['words', '0x016e3600'],
	'mclk' : ['strings', 'clk_out_3'],
	'reset-gpios' : ['words', '0x00000078', '0x00000094', '0x00000000'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x00000130'],
	'phandle' : ['words', '0x00000130'],
	'mode0' : {
		'mclk_khz' : ['strings', '24000'],
		'num_lanes' : ['strings', '4'],
		'tegra_sinterface' : ['strings', 'serial_a'],
		'discontinuous_clk' : ['strings', 'no'],
		'dpcm_enable' : ['strings', 'false'],
		'cil_settletime' : ['strings', '0'],
		'dynamic_pixel_bit_depth' : ['strings', '8'],
		'csi_pixel_bit_depth' : ['strings', '8'],
		'mode_type' : ['strings', 'bayer'],
		'pixel_phase' : ['strings', 'rggb'],
		'active_w' : ['strings', '3456'],
		'active_h' : ['strings', '2580'],
		'readout_orientation' : ['strings', '0'],
		'line_length' : ['strings', '3456'],
		'inherent_gain' : ['strings', '1'],
		'mclk_multiplier' : ['strings', '8'],
		'pix_clk_hz' : ['strings', '182400000'],
		'min_gain_val' : ['strings', '1.0'],
		'max_gain_val' : ['strings', '100'],
		'min_hdr_ratio' : ['strings', '1'],
		'max_hdr_ratio' : ['strings', '64'],
		'min_framerate' : ['strings', '2.2290'],
		'max_framerate' : ['strings', '20'],
                'min_exp_time' : ['strings', '44'],
		'max_exp_time' : ['strings', '33000'],
		'embedded_metadata_height' : ['strings', '0'],
	},
	'ports' : {
            '#address-cells' : ['words', '0x00000001'],
            '#size-cells' : ['words', '0x00000000'],
	    'port@0' : {
	        'reg' : ['words', '0x00000000'],
	        'endpoint' :{
		    'csi-port' : ['words', '0x0'],
		    'bus-width' : ['words', '0x4'],
		    'remote-endpoint' : ['words', '0x00000093'],
		    'linux,phandle' : ['words', '0x00000131'],
		    'phandle' : ['words', '0x00000131'],
		},
	    },
	},
    };

    imx185_b = {
	'compatible' : ['strings', 'nvidia,imx185'],
	'reg' : ['words', '0x0000001b'],
	'physical_w' : ['strings', '15.0'],
	'physical_h' : ['strings', '12.5'],
	'sensor_model' : ['strings', 'imx185'],
	'post_crop_frame_drop' : ['strings', '0'],
	'use_decibel_gain' : ['strings', 'true'],
	'delayed_gain' : ['strings', 'true'],
	'use_sensor_mode_id' : ['strings', 'true'],
	'clocks' : ['words', '0x00000041', '0x00000117'],
	'clock-names' : ['strings', 'clk_out_3'],
	'clock-frequency' : ['words', '0x016e3600'],
	'mclk' : ['strings', 'clk_out_3'],
	'reset-gpios' : ['words', '0x00000078', '0x00000094', '0x00000000'],
	'status' : ['strings', 'okay'],
	'linux,phandle' : ['words', '0x00000130'],
	'phandle' : ['words', '0x00000130'],
	'mode0' : {
		'mclk_khz' : ['strings', '24000'],
		'num_lanes' : ['strings', '2'],
		'tegra_sinterface' : ['strings', 'serial_c'],
		'discontinuous_clk' : ['strings', 'no'],
		'dpcm_enable' : ['strings', 'false'],
		'cil_settletime' : ['strings', '0'],
		'dynamic_pixel_bit_depth' : ['strings', '8'],
		'csi_pixel_bit_depth' : ['strings', '8'],
		'mode_type' : ['strings', 'bayer'],
		'pixel_phase' : ['strings', 'rggb'],
		'active_w' : ['strings', '3456'],
		'active_h' : ['strings', '2580'],
		'readout_orientation' : ['strings', '0'],
		'line_length' : ['strings', '3456'],
		'inherent_gain' : ['strings', '1'],
		'mclk_multiplier' : ['strings', '8'],
		'pix_clk_hz' : ['strings', '182400000'],
		'min_gain_val' : ['strings', '1.0'],
		'max_gain_val' : ['strings', '100'],
		'min_hdr_ratio' : ['strings', '1'],
		'max_hdr_ratio' : ['strings', '64'],
		'min_framerate' : ['strings', '2.2290'],
		'max_framerate' : ['strings', '20'],
                'min_exp_time' : ['strings', '44'],
		'max_exp_time' : ['strings', '33000'],
		'embedded_metadata_height' : ['strings', '0'],
	},
	'mode1' : {
		'mclk_khz' : ['strings', '24000'],
		'num_lanes' : ['strings', '2'],
		'tegra_sinterface' : ['strings', 'serial_c'],
		'discontinuous_clk' : ['strings', 'no'],
		'dpcm_enable' : ['strings', 'false'],
		'cil_settletime' : ['strings', '0'],
		'dynamic_pixel_bit_depth' : ['strings', '8'],
		'csi_pixel_bit_depth' : ['strings', '8'],
		'mode_type' : ['strings', 'bayer'],
		'pixel_phase' : ['strings', 'rggb'],
		'active_w' : ['strings', '2400'],
		'active_h' : ['strings', '1080'],
		'readout_orientation' : ['strings', '0'],
		'line_length' : ['strings', '2400'],
		'inherent_gain' : ['strings', '1'],
		'mclk_multiplier' : ['strings', '8'],
		'pix_clk_hz' : ['strings', '182400000'],
		'min_gain_val' : ['strings', '1.0'],
		'max_gain_val' : ['strings', '100'],
		'min_hdr_ratio' : ['strings', '1'],
		'max_hdr_ratio' : ['strings', '64'],
		'min_framerate' : ['strings', '2.2290'],
		'max_framerate' : ['strings', '20'],
                'min_exp_time' : ['strings', '44'],
		'max_exp_time' : ['strings', '33000'],
		'embedded_metadata_height' : ['strings', '0'],
	},
        'ports' : {
            '#address-cells' : ['words', '0x00000001'],
            '#size-cells' : ['words', '0x00000000'],
	    'port@0' : {
	        'reg' : ['words', '0x00000000'],
	        'endpoint' :{
		    'csi-port' : ['words', '0x2'],
		    'bus-width' : ['words', '0x2'],
		    'remote-endpoint' : ['words', '0x00000094'],
		    'linux,phandle' : ['words', '0x00000132'],
		    'phandle' : ['words', '0x00000132'],
		},
	    },
	},
    };
     
    vii2c_node = fdt.resolve_path('/host1x/i2c@546c0000')
    add(vii2c_node, 'imx185_a@1a', imx185_a)
    add(vii2c_node, 'imx185_b@1b', imx185_b)

    # add spi0 spi1            
    spijson = {
        '#address-cells': ['words', '0x1'],
        '#size-cells': ['words', '0x0'],
        'compatible': ['strings', "spidev"],
        'reg': ['words', '0x0'],
        'spi-max-frequency': ['words', '20000000'],
        'nvidia,enable-hw-based-cs': None,
        'nvidia,cs-setup-clk-count':['words', '0x1e'],
        'nvidia,cs-hold-clk-count': ['words', '0x1e'],
        'nvidia,rx-clk-tap-delay': ['words', '0x1f'],
        'nvidia,tx-clk-tap-delay': ['words', '0x0']
    };

    node = fdt.resolve_path('/spi@7000d400')
    add(node, 'spi1_0', spijson)

    node = fdt.resolve_path('/spi@7000da00')  # jetson SPI0 -> SPI4
    add(node, 'spi0_0', spijson)
    add(node, 'status', ['strings', 'okay'])


    # set pinmux (pcie & usb)
    node = fdt.resolve_path('/pinctrl@7009f000/pinmux')
    node.remove('usb2-std-A-port0')
    node.remove('usb2-micro-AB-xusb')
    node.remove('usb3-std-A-port0')
    node.remove('usb2-eth')
    node.remove('usb3-eth')
    node.remove('pcie-m2')
    node.remove('sata')
    node.remove('pcie')

    add(node, 'usb2-mon', {
        'nvidia,function' : ['strings', 'xusb'],
        'nvidia,lanes' : ['strings', 'otg-2'],
        'nvidia,port-cap' : ['words', '0x1']   # TEGRA_PADCTL_PORT_HOST_ONLY
    })
    add(node, 'usb2-typec', {
        'nvidia,function' : ['strings', 'xusb'],
        'nvidia,lanes' : ['strings', 'otg-0'],
        'nvidia,port-cap' : ['words', '0x3']    # TEGRA_PADCTL_PORT_OTG_CAP
    })
    add(node, 'usb3-typec', {
        'nvidia,function' : ['strings', 'usb3'],
        'nvidia,lanes' : ['strings', 'uphy-lane-5'],   # USB_SS0
        'nvidia,port-cap' : ['words', '0x3'],   # TEGRA_PADCTL_PORT_OTG_CAP
        'nvidia,usb2-map' : ['words', '0x0'],
        'nvidia,usb3-port' : ['words', '0x0']
    })
    add(node, 'pcie', {
        'nvidia,function' : ['strings', 'pcie'],
        'nvidia,lanes' : ['strings', 'uphy-lane-0'],  # PEX1
        'nvidia,pcie-controller' : ['words', '0x1'],
        'nvidia,pcie-lane-select' : ['words', '0x1'],  # TEGRA_PADCTL_PCIE_LANE_X1
    })
    
    node = fdt.resolve_path('/pcie-controller@1003000')
    node.remove('pci@1,0')
    
    node = fdt.resolve_path('/xusb@70090000')
    add(node, 'phys', 'words 0x5b 0x12 0x5b 0x00 0x5b 0x10'.split(' '))
    add(node, 'phy-names', 'strings utmi-2 usb3-0 utmi-0'.split(' '))
    
    node = fdt.resolve_path('/regulators')
    node.remove('regulator@208') # USB1_EN_OC  /  en-usb-vbus2
    node.remove('regulator@14') # USB0_EN_OC  /  usb-vbus1
    
    if printdts:
    	print fdt.to_dts()
    return fdt.to_dtb()

# the algorithm is coming from 
# Tegra BCT and bootable flash image generator/compiler
# https://github.com/NVIDIA/cbootimage
def signdata(data):
    try:
        from Crypto.Hash import CMAC
        from Crypto.Cipher import AES
        
        secret = b'\0'*16
        cobj = CMAC.new(secret, ciphermod=AES)
        
        cobj.update(data)
        return cobj.digest()
        
    except:
        print 'ERROR: pycryptodome is not installed. please run following commands'
        print 'sudo pip uninstall pycrypto'
        print 'sudo pip install pycryptodome'
        sys.exit(2)
    
def flashdtb(dtb):
    dtb += '\x80\x00'
    while len(dtb) % 16 != 0:
        dtb += '\x00'
    sz = len(dtb) + 0x400
    magic = '\xb1\xba\xef\xbe\xad\xde\xed\xfe'
    dtb = '\xcc'*16 + struct.pack('<I', sz) + '\x00' * 4 + magic * 2 + '\x00' * 424 + dtb
    dtb = '\x00\x00\x00\x00DTB\x00' + struct.pack('<I', sz) + '\x00' * 4 + magic + '\xaa' * 256 + signdata(dtb) + '\xee' * 256 + '\x00' * 8 + dtb
    
    for n in range(30):
        try:
            fname = '/dev/mmcblk0p%d' % n
            
            with open(fname, 'rb') as f:
                b = f.read(0x18)
                if b[4:7] != 'DTB' or b[0x10:] != magic:
                    continue
            print 'DTB @', fname
            with open(fname, 'wb') as f:
                f.write(dtb)
        except:
            pass

            
def resetusb():
    import usb.core, fcntl
    
    for id in [(0x4338, 0x55e0), (0x16c0, 0x05e1)]:
        dev = usb.core.find(idVendor=0x4348, idProduct=0x55e0)
        if dev is None: continue
        path = '/dev/bus/usb/%03d/%03d' % (dev.bus, dev.address)
        with open(path, 'w') as f:
            USBDEVFS_RESET = 0x5514
            fcntl.ioctl(f, USBDEVFS_RESET, 0)
    del usb

    
usage=('-startpm', '-rebootpm', '-flashdtb', '-printdtb', '-genko', '-resetusb', '*.dtb', '*.ihx')
def showusage():
    print 'usage:'
    for s in usage:
        print '\t./tegra.py %s' % s.replace('*', 'filename')
    sys.exit(1)

if len(sys.argv) < 2: showusage()
    
if os.getuid() != 0:
    print 'NOT root user'
    sys.exit(1)

MDOC = 229
MCUPDN = 66
MCUPROG = 62
if sys.argv[1] in (usage[0], usage[1]):
    from fc2jtag import FC2GPIO
    
    gpio = FC2GPIO()
    gpio.mode(MCUPDN, gpio.OUTPUT)
    
    if sys.argv[1] == usage[0]:
        gpio.clr(MCUPDN)
    else:
        gpio.set(MCUPDN)
        time.sleep(0.3)
        gpio.clr(MCUPDN)

        
elif sys.argv[1] == usage[2]:
    flashdtb( gendtb(0) )

elif sys.argv[1] == usage[3]:
    gendtb(1)

elif sys.argv[1] == usage[4]:
    genko()

elif sys.argv[1] == usage[5]:
    resetusb()
    
elif sys.argv[1].endswith('.dtb'):
    with open(sys.argv[1], 'rb') as f:
        flashdtb(f.read())
    
elif sys.argv[1].endswith('.ihx'):
    from fc2jtag import FC2GPIO
    gpio = FC2GPIO()
    
    gpio.mode(MCUPDN, gpio.OUTPUT)
    gpio.mode(MCUPROG, gpio.OUTPUT)
    
    # power off/on PM in FLASH mode
    gpio.clr(MCUPROG)
    gpio.set(MCUPDN)
    time.sleep(0.3)
    gpio.clr(MCUPDN)
    time.sleep(0.3)
    resetusb()
    time.sleep(0.3)
    
    try:
        isp = WCHISP()
        if isp.info() != 0x52:
            raise IOException("not a CH552T device")
        isp.program(sys.argv[1])
        del isp
        
    finally:
        # power off/on PM in NORMAL mode
        gpio.set(MCUPDN)
        gpio.mode(MCUPROG, gpio.INPUT)
        time.sleep(0.3)
        gpio.clr(MCUPDN)
        time.sleep(0.5)
        resetusb()
        
else:        
    showusage()

sys.exit(0)
