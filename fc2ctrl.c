#include "fc2ctrl.h"

#include <zlib.h>
#include <jpeglib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SDECODE_IMP
#include "gpio.h"
#include "csi.h"
#include "spi.h"
#include "gst.h"

int seconds, vsize, vline, vtype, dline, framecnt;

void scput(MSGCHNL* mchnl, const MSGHDR* msg)
{
    RXCHNL* chnl = (RXCHNL*)mchnl->ext;
    if(chnl->recv)(*chnl->recv)(chnl, msg->data, msg->idx+1, msg->flags);
    chnl->dcnt += msg->idx/4;
}

static void wavequeuerecv(RXCHNL*chnl, const uint32_t*data, int size, int flags)
{
    if(!chnl->q)return;
    
    const bqueue_entry* qe = bqueue_current(chnl->q);
	bqueue_append(chnl->q, (const uint8_t*)data, size);
    if(qe->length >= chnl->q->bsize){
	    fprintf(stderr, "queue head entry[%p] length: %u, reach size limit: %u\n", qe, qe->length, chnl->q->bsize);
		bqueue_produce(chnl->q);
	}
}

static void waverecv(RXCHNL*chnl, const uint32_t*data, int size, int flags)
{
    struct WaveHeader
    {
        unsigned char  riff[4];
        unsigned int   lChunkSize;
        unsigned char  wave[4];
        unsigned char  fmtChunkId[4];
        unsigned int   fmtChunkSize;
        unsigned short audioFormat;
        unsigned short numChannels;
        unsigned int   sampleRate;
        unsigned int   byteRate;
        unsigned short blockAlign;
        unsigned short bitsBySample;
        unsigned char  fmtDataId[4];
        unsigned int   dataChunkSize;
    } waveHeader;

    off_t off = lseek(chnl->fd, 0, SEEK_CUR);
    if(off == 0)write(chnl->fd, &waveHeader, sizeof(waveHeader));
    
    if(size){
        write(chnl->fd, data, size);
    } else {
        memcpy((void*)waveHeader.riff, "RIFF", 4);
        memcpy((void*)waveHeader.wave, "WAVE", 4);
        memcpy((void*)waveHeader.fmtChunkId, "fmt ", 4);
        waveHeader.fmtChunkSize = 16;
        waveHeader.audioFormat  =  1;
        waveHeader.numChannels  =  1;
        waveHeader.sampleRate   = 48000;
        waveHeader.byteRate     = 96000;
        waveHeader.blockAlign   = 2;
        waveHeader.bitsBySample = 16;
        memcpy((void*)waveHeader.fmtDataId, "data", 4);
        waveHeader.dataChunkSize = off - sizeof(waveHeader);
        waveHeader.lChunkSize = off - sizeof(waveHeader) + 32;
        lseek(chnl->fd, 0, SEEK_SET);
        write(chnl->fd, &waveHeader, sizeof(waveHeader));
    }
}

static void datarecv(RXCHNL*chnl, const uint32_t*data, int size, int flags)
{
}

static void tdatarecv(RXCHNL*chnl, const uint32_t*data, int size, int flags)
{
    int cnt = 0;
    int lost = 0;
    
    int off = chnl->vars[0];
    int last = chnl->vars[1];
    for(int i=0; i<size/4; i++){
        if(off%16 == 0){
            int v = data[i] & 0xffff;
            if(v != ((last + 1) & 0xffff)) {
                fprintf(stderr, "%x->(%d:%x) ", last, i, data[i]&0xffff);
                lost ++;
            }
            last = v;
        }
        off += 1;
    }
    chnl->vars[0] = off;
    chnl->vars[1] = last;
    //if(lost) fprintf(stderr, "%d/%d ", lost, size/4);
}

static void trawrecv(RXCHNL*chnl, const uint32_t*data, int size, int y)
{
    static uint16_t frame;
    if(y != 0)return;
    
    if((data[0]>>16) != 0x4241 && (data[vline/4]>>16) != 0x4241){
        fprintf(stderr, "! ");
        return;
    }
    
    if((data[0]>>16) != 0x4241 || ((data[vline/4]>>16) != 0x4241 && (data[vline/4]>>16) != 0x4001)){
        fprintf(stderr, "%08x %08x ", data[0], data[vline/4+1]);
        fprintf(stderr, "C ");
        return;
    }
    bool f = frame != (data[0]&0xffff);
    frame = (data[0] + 1)&0xffff;
    if(f){
        fprintf(stderr, "F ");
        return;
    }
    
    for(int n=1; n<vsize/vline-3; n++){
        uint32_t va = data[n*vline/4 + 0x4];
        uint32_t vb = data[n*vline/4 + 0x37];
        if((va & 0xffff) != 0x8005 || (vb & 0xffff) != 0x8038){
            fprintf(stderr, "X%d ", n);
            return;
        }
        if((va>>16) != (0x4000 + n) || (vb>>16) != (0x4000 + n)){
            fprintf(stderr, "Y%d ", n);
            return;
        }
    }
    fprintf(stderr, "%02x ", data[0]&0xff);
}


static void awbrecv(RXCHNL*chnl, const uint32_t*data, int size, int flags)
{
    if(!chnl->q)return;
    
    static uint16_t cnts[4];
    
    int yid = ((data[0]>>24)-1)&0x1f;
    int cnt = (((data[2]>>24)&0xff)<<8) + ((data[1]>>24)&0xff);
    
    int rx = (data[0]>>29)&3;
    if(cnt != (cnts[rx]+1)&0xffff){
        fprintf(stderr, "awb[%d] %d->%d\n", framecnt, cnts[rx], cnt);
    }
    cnts[rx] = cnt;
    
    if(yid == 0) chnl->q->wptr ^= 1<<rx;
    rx = rx*2 + ((chnl->q->wptr >> rx)&1);
    
    bqueue_entry* qe = chnl->q->data[rx];
    memcpy((uint32_t*)&qe->mem[yid*32*12], data, size);
}

static int awbcompare0(const void*arg1, const void*arg2){
    uint32_t v1 = ((const uint32_t*)arg1)[0]&0xfff;
    uint32_t v2 = ((const uint32_t*)arg2)[0]&0xfff;
    if(v1 == v2)return 0;
    if(v1 < v2)return 1;
    return -1;
}
static int awbcompare1(const void*arg1, const void*arg2){
    uint32_t v1 = (((const uint32_t*)arg1)[0]>>12)&0xfff;
    uint32_t v2 = (((const uint32_t*)arg2)[0]>>12)&0xfff;
    if(v1 == v2)return 0;
    if(v1 < v2)return 1;
    return -1;
}

static void setgain(int n, int gain)
{
    static uint32_t cmds[] = {
        0xfc080004, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0xfc6e0000, 0xfc505004, 0xfc080103,
        0x0006a201, 0x04022054, 0x00006400, 0xfc505103,
        0xfc505004, 0x00000000};
    cmds[8] = (cmds[8]&0xffffff00) | (1<<n);
    cmds[10] = (gain>>8) | ((gain&0xff)<<8);
    spictrl(cmds, sizeof(cmds)/sizeof(cmds[0]), -1, NULL);
}

static void awbrun(RXCHNL*chnl)
{
    for(int n=0; n<4; n++)if((chnl->q->rptr ^ chnl->q->wptr) & (1<<n)){
        chnl->q->rptr ^= 1<<n;
        
        int rx = n*2 + ((chnl->q->wptr >> n)&1);
        bqueue_entry* qe = chnl->q->data[rx];
        
        int wcnt=0;  // white pxs count
        int bcnt=0;  // black pxs count
        int rg=0;
        int bg=0;

        qsort(qe->mem, 32*32, 12, awbcompare0);
        uint32_t* pmem = (uint32_t*)&qe->mem[0];
        for(int i=0; i<64; i++){
            wcnt += pmem[i*3]&0xfff;
            rg += pmem[i*3+1]&0xfffff;
            bg += pmem[i*3+2]&0xfffff;
        }
        
        qsort(qe->mem, 32*32, 12, awbcompare1);
        for(int i=0; i<64; i++){
            bcnt += (pmem[i*3]>>12)&0xfff;
        }
        if(wcnt <= 100 && bcnt <= 100) continue;
        if(wcnt == 0) wcnt = 1;
        rg /= wcnt;
        bg /= wcnt;
        if(rg>63 || bg>63)continue; // invalid data
        
        wcnt = (wcnt*32+bcnt/2)/bcnt;  // to ratio
        if(wcnt>63) wcnt = 63;
        
        // wcnt, rg, bg  [0-63]
        uint8_t* history = &chnl->q->data[n]->mem[32*32*12];
        uint8_t c = *history;
        c = (c+1)&15;
        *history = c;
        
        history += 1 + c*3;
        
        history[0] = wcnt;
        history[1] = rg;
        history[2] = bg;
        
        chnl->vars[0] += 1;
    }
    
    if(chnl->vars[0] >= 15){
        chnl->vars[0] = 0;
        
        uint16_t stat[4][6];
        memset(stat, 0, sizeof(stat));
        
        for(int n=0; n<4; n++){
            uint8_t* history = &chnl->q->data[n]->mem[32*32*12];
            uint8_t c = *history++;
            c -= 4;
            for(int i=0; i<4; i++){ // level by 4
                c = (c+1)&15;
                stat[n][0] += history[c*3+0];
                stat[n][1] += history[c*3+1];
                stat[n][2] += history[c*3+2];
            }
            for(int i=0; i<16; i++){ // level by 16
                stat[n][3] += history[i*3+0];
                stat[n][4] += history[i*3+1];
                stat[n][5] += history[i*3+2];
            }
            stat[n][3] /= 4;
            stat[n][4] /= 4;
            stat[n][5] /= 4;
        }
        
        uint16_t v = 0;
        int c=0;
        for(int n=0; n<4; n++)if(stat[n][3]){
            v += stat[n][3];
            c += 1;
        }
        if(c == 0)return;
        v /= c;
        
        if(chnl->vars[2] == 0)chnl->vars[2] = 3000;  // default gain
        
        // setgain for v -> MID
        const int MID = 90;
        if(v>MID-8 && v<MID+8)return;
        c = chnl->vars[2] * (v-MID) / 256;
        
        chnl->vars[2] -= c;
        if(chnl->vars[2] < 1000) chnl->vars[2] = 1000;
        if(chnl->vars[2] > 8000) chnl->vars[2] = 8000;
        
        for(int n=0; n<4; n++)if(stat[n][0] != stat[n][3] || stat[n][1] != stat[n][4] || stat[n][2] != stat[n][5]){
            c = ((MID - stat[n][0]) * chnl->vars[2])/512 + chnl->vars[2];
            if(c < 1000) c = 1000;
            if(c > 8000) c = 8000;
            c = 512 - (512000/c);
            setgain(n, c);
            
            fprintf(stderr, "setgain %d %d\n", n, c);
        }
        
        //fprintf(stderr, "AWB ");
        //fprintf(stderr, "<%d/%d %d/%d %d/%d> ", wcnt, wcnt8, rg, rg8, bg, bg8);
        //fprintf(stderr, "\n");
    }
}

static void jpegrecv(RXCHNL*chnl, const uint32_t*data, int size, int flags)
{
    if(!chnl->q)return;
    
    uint16_t hdrlen = chnl->info[0]&0xffff;
    
    const bqueue_entry* qe = bqueue_current(chnl->q);
    if(qe->length == 0){
        bqueue_append(chnl->q, (const uint8_t*)&chnl->info[1], hdrlen);
    }
    /*
    if(qe->length == hdrlen && (data[0]&0xffffff) == 0){ // debug jpeg stream
        chnl->vars[0] = 0x100 + (data[0]>>24);
        chnl->vars[1] = data[0] + size/4;
    } else if(chnl->vars[0] == 0x100 + (data[0]>>24)){
        if(chnl->vars[1] != data[0])fprintf(stderr, "J(%x->%x@%d) ", chnl->vars[1], data[0], qe->length);
        chnl->vars[1] = data[0] + size/4;
    }*/
    bqueue_append(chnl->q, (const uint8_t*)data, size);
    
    if(flags == 1){
        const uint8_t * exinfo = &qe->mem[qe->length-8];
        int elen = (exinfo[3]<<16) | (exinfo[2]<<8) | exinfo[1];
        int len = qe->length - 8 - hdrlen;
        if(exinfo[0] != 0xc9 || elen != len){
            fprintf(stderr, "JPEG: elen %d!=%d\n", elen, len);
            bqueue_setlength(chnl->q, hdrlen);  // remain SOI
            return;
        } 
        unsigned ecrc = (exinfo[7]<<24) | (exinfo[6]<<16) | (exinfo[5]<<8) | exinfo[4];
        unsigned crc = crc32(0, &(qe->mem[hdrlen]), len);
        if(crc != ecrc){
            fprintf(stderr, "JPEG: CRC %x!=%x\n", crc, ecrc);
            bqueue_setlength(chnl->q, hdrlen); // remain SOI
            return;
        }
        bqueue_setlength(chnl->q, qe->length - 8);  // discard exinfo
        
        hdrlen = (hdrlen&3) ? (hdrlen/4+1) : (hdrlen/4);
        bqueue_append(chnl->q, (const uint8_t*)&chnl->info[1+hdrlen], chnl->info[0]>>16);
        if( !bqueue_produce(chnl->q) ){
            bqueue_setlength(chnl->q, 0);
            fprintf(stderr, "JPEG: fifo overflow\n");
        }
    }
}

static void jpegrun(RXCHNL*chnl)
{
    while(1){
        const bqueue_entry* e = bqueue_peek(chnl->q);
        if(e == NULL)break;
        
        if(chnl->fd >0){ // to MJPEG
            if(chnl->vars[0] == 0){
                static const char* hdr = "Cache-Control:no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0\r\n"
                    "Connection:close\r\n"
                    "Content-Type:multipart/x-mixed-replace;boundary=boundarydonotcross\r\n"
                    "Expires:Thu, 29 Mar 2018 12:00:00 GMT\r\n"
                    "Pragma:no-cache\r\n"
                    "Server:MJPG-Streamer/0.2\r\n\r\n";
                write(chnl->fd, hdr, strlen(hdr));
            }
            
            chnl->vars[0] += 1;
            if(bqueue_count(chnl->q) > 2){
                //fprintf(stderr, "skip frame[%d] %d\n", chnl->vars[0], e->length);
                bqueue_consume(chnl->q);
                continue;
                
            } else {
                char bstr[256];
                sprintf(bstr, "--boundarydonotcross--\r\nContent-Type: image/jpeg\r\nContent-Length: %d\r\nX-Timestamp: %d\r\n\r\n", e->length, chnl->vars[0]*9000);
                if( write(chnl->fd, bstr, strlen(bstr)) <= 0 || write(chnl->fd, e->mem, e->length) <= 0 ){
                    perror("write");
                    exit(0);
                }
                bqueue_consume(chnl->q);
                break;
            }
        }
        
        OBUF* obuf = obufget();
        if(obuf == NULL){
            // drop current frame
            bqueue_consume(chnl->q);
            break;
        }
    
        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;
        
        cinfo.err = jpeg_std_error(&jerr);	
        jpeg_create_decompress(&cinfo);
        jpeg_mem_src(&cinfo, (uint8_t*)e->mem, e->length);
        
        if( jpeg_read_header(&cinfo, TRUE) == 1){
            int h = 1078;
            if(cinfo.output_height < h) h = cinfo.output_height;
            jpeg_start_decompress(&cinfo);
            //fprintf(stderr, "JPEG %u x %u %d\n", cinfo.image_width, cinfo.image_height, cinfo.output_scanline);
            while (cinfo.output_scanline < h) {
                unsigned char *buffer_array[1];
                buffer_array[0] = obuf->data + cinfo.output_scanline*1920*3;
                jpeg_read_scanlines(&cinfo, buffer_array, 1);
            }
            if(h < cinfo.output_height){
                jpeg_abort_decompress(&cinfo);
            } else {
                jpeg_finish_decompress(&cinfo);
            }
            
            obuf->length = 1920*1080*3;
            obufsend(obuf);
            
        } else {
            obufrelease(obuf);
        }
        jpeg_destroy_decompress(&cinfo);
        
        bqueue_consume(chnl->q);
    }
}


static char* scparams[16];
unsigned scinit(unsigned id, MSGCHNL* mchnl)
{
    char* msg = scparams[id];
    if(!msg)return 0;
    
    RXCHNL* chnl = (RXCHNL*)mchnl->ext;
    chnl->info = getoutinfo(id, &chnl->type);
    if(!chnl->info){
        fprintf(stderr, "W: no chnl[%d]\n", id);
        return 0;
    }
    
    char* ext;
    if(msg[0] == '-'){ // redir
        if(msg[1] == 0) 
            chnl->fd = -1;
        else {
            chnl->fd = atoi(&msg[1]);
            if(fcntl(chnl->fd, F_GETFD) < 0){
                fprintf(stderr, "E: invalid fd %d\n", chnl->fd);
                return 0;
            }
        }
    } else if((ext=strchr(msg, ':')) != NULL){ // socket
        *ext++ = 0;
        int sock = socket(AF_INET, SOCK_STREAM, 0);
        if(sock < 0){
            perror("socket");
            return 0;
        }
        
        chnl->vars[1] = 1000;
        
        struct sockaddr_in sin;
        memset(&sin, 0, sizeof(sin));
        sin.sin_family = AF_INET;
        sin.sin_port = htons(atoi(ext));
        if(msg[0]){ // connect to
            if(inet_pton(AF_INET, msg, &sin.sin_addr)<=0){
                perror("inet_pton");
                return 0;
            }
            if( connect(sock, (struct sockaddr*)&sin, sizeof(sin)) < 0){
                perror("connect");
                return 0;
            }
            chnl->fd = sock;
            
        } else { // bind & wait connection
            sin.sin_addr.s_addr = htonl(INADDR_ANY);
            int n = 1;
            setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &n, sizeof(n));
            if( bind(sock, (struct sockaddr*)&sin, sizeof(sin)) < 0){
                perror("bind");
                return 0;
            }
            listen(sock, 1);
            fprintf(stderr, "Wait on tcp port %s ... ", ext);
            fflush(stderr);
            n = sizeof(sin);
            chnl->fd = accept(sock, (struct sockaddr*)&sin, &n);
            close(sock);
            fprintf(stderr, "connected\n");
        }
        
    } else {
        chnl->fd = open(msg, O_WRONLY | O_CREAT, 0664);
        if(chnl->fd < 0){
            perror("open");
            return 0;
        }
        if( ftruncate(chnl->fd, 0) != 0 ) {
            perror("ftruncate");
            return 0;
        }
    }
    
    if(chnl->type == 0){// PCM buffer
        //chnl->recv = waverecv;
        chnl->recv = wavequeuerecv;
        chnl->q = bqueue_init(1024*8, 8);
		audiopipeline_init(chnl);
    } else if(chnl->type == 1){ // JPEG buffer
        chnl->recv = jpegrecv;
        chnl->run = jpegrun;
        chnl->q = bqueue_init(vsize/3, 16);
        videopipeline_init(chnl);
        return 256;
    } else if(chnl->type == 2){ // AWB buffer
        chnl->recv = awbrecv;
        chnl->run = awbrun;
        chnl->q = bqueue_init(32*32*12+200, 8);
        return 128;
    } else if(chnl->type == 0xa || chnl->type == 0x8){ // RAW buffer
        chnl->recv = trawrecv;
    } else if(chnl->type == 0xe){ // test data buffer
        chnl->recv = tdatarecv;
    } else {
        chnl->recv = datarecv;
    }
    return 64;
}

void scquit(MSGCHNL* mchnl)
{
    RXCHNL* chnl = (RXCHNL*)mchnl->ext;
    
    if(chnl->fd>0 && chnl->type == 0){ // update wavefile header
        (*chnl->recv)(chnl, NULL, 0, 0);
    }
    if(chnl->q)bqueue_free(chnl->q);
}


int main(int argc, char **argv) {
    if(argc < 2) goto argerror;
    
    int mode = 0;
    if(strcmp(argv[1], "-f") == 0) mode = 1;
    if(strcmp(argv[1], "-on") == 0) mode = 2;
    if(strcmp(argv[1], "-off") == 0) mode = 3;
    if(strcmp(argv[1], "-r") == 0) mode = 4;
    
    if(mode)return fpgamode(mode);
    
    if(strcmp(argv[1], "-rx") == 0 && argc >= 3){
        seconds = atoi(argv[2]);
        
        int w, h;
        //get raw image info from bin file?
        const uint32_t * rawinfo = getoutinfo(15, &vtype);
        if((vtype != 8 && vtype != 10 && vtype != 15) || rawinfo == NULL){
            fprintf(stderr, "E: get output resolution\n");
            return 1;
        }
        w = rawinfo[0]&0xffff;
        h = rawinfo[0]>>16;
        if(w<80 || h<80 || w>16384 || h>8192 || (w&15) != 0 || (h&7) != 0){
            fprintf(stderr, "E: invalid resolution %dx%d\n", w, h);
            return 2;
        }
        csisetsize(w, h);
        fprintf(stderr, "CSI %dx%d, TYPE=%X, VLINE=%d, DLINE=%d\n", w, h, vtype, vline, dline);
        
        memset(scparams, 0, sizeof(scparams));
        for(int i=3; i<argc; i++){
            if(argv[i][0] == '-'){
                if(argv[i][1] == 't'){
                    for(int j=0; j<9; j++){
                        if(argv[i][j+2] < '0')break;
                        if(argv[i][j+2] >= '0' && argv[i][j+2] <= '9') rxtiming[j] = argv[i][j+2] - '0';
                        if(argv[i][j+2] >= 'A' && argv[i][j+2] <= 'F') rxtiming[j] = argv[i][j+2] - 'A'+10;
                    }
                }
                continue;
            }
            char* msg = strchr(argv[i], ':');
            if(msg == NULL){
                fprintf(stderr, "E: argv[%d] bad output format\n", i);
                return 1;
            }
            *msg++ = 0;
            int id = 15;
            if(argv[i][0] != 0) id = atoi(argv[i]);
            scparams[id] = msg;
        }

        if(!sinit(sizeof(RXCHNL)/sizeof(unsigned))){
            fprintf(stderr, "E: sinit malloc()\n");
            return 1;
        }
        return csirun(w, h);
    }
    
    if(argc >= 3 && strcmp(argv[1], "-mr")==0){
        uint32_t addr = strtoul(argv[2], NULL, 16);
        return readmem(addr);
    }
    if(argc >= 4 && strcmp(argv[1], "-mw")==0){
        return writemem(argv[2], argv[3]);
    }
    
    if(strstr(argv[1], ".bin") != 0){
        static uint32_t modify[17];
        int i = 0;
        int cnt = 0;
        int ret = 0;
        
        for(; i<8; i++){
            char* eq;
            if(i+2>=argc)break;
            if(argv[i+2][0] == '-'){
                cnt = (argv[i+2][1] == 'v') ? 1 : (argv[i+2][1] == 't') ? 2 : atoi(&argv[i+2][1]);
            }
            eq = strchr(argv[i+2], '=');
            if(eq == NULL)continue;
            *eq = 0;
            
            modify[i*2] = strtoul(argv[i+2], NULL, 16);
            modify[i*2+1] = strtoul(eq+1, NULL, 16);
        }
        modify[i*2] = 0;
        
        if(cnt < 3) {
            spiverbose = cnt;
            cnt = 0;
        }
        do {
            ret = spirun(argv[1], modify);
            cnt -= 1;
            if(cnt >= 0) sleep(1);
        } while(cnt >= 0);
        return ret;
    }
    
argerror:    
    fprintf(stderr, "usage: %s -r | -f | -on | -off | [-v] binfile | -mr hexaddr | -rx timeout output0 output1 ...\n", *argv);
    return 1;
}
