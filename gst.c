#include "gst.h"
#include <time.h>

#define CHUNK_SIZE 1024   /* Amount of bytes we are sending in each buffer */
#define SAMPLE_RATE 48000 //opus default 

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _AudioContext {
	GstElement *audio_convert1;
	GstElement *audio_resample;
	GstElement *opus_enc;
	GstElement *rtp_opuspay; 
	GstElement *udp_sink;
} AudioContext;

typedef struct _VideoContext {
	GstElement *jpeg_dec;
	GstElement *h264_enc;
	GstElement *rtp_h264pay;
	GstElement *udp_sink;
} VideoContext;

typedef struct _AppsrcContext {
	GstElement *pipeline;
	GstElement* app_source;
    AudioContext* audioCtx;
    VideoContext* videoCtx;    
	RXCHNL *chnl;
	bool busy;
} AppsrcContext;

static GstAppSrcCallbacks srccbs;
static GstAudioInfo audioInfo;

static void feed(AppsrcContext* c)
{
	while(!c->busy){
	    const bqueue_entry* e = bqueue_peek(c->chnl->q);
		//LOG_DEBUG("box appsrc[%p] feed %d size=%d", appsrc.p, f->type, f->size);

		if(e != NULL)
		{
			fprintf(stderr, "gst audio pipeline got data from queue\n");
			GstBuffer *buf = gst_buffer_new_wrapped(g_memdup((uint8_t*)e->mem, e->length), e->length); 
			//TODO: timestamp it
			/*if(fps){
			  GST_BUFFER_PTS(buf) = (uint64)(fcnt * 1000/fps) * GST_MSECOND;
			  GST_BUFFER_DURATION(buf) = 1000/fps*GST_MSECOND;
			  } else {
			  GST_BUFFER_PTS(buf) = f->timestamp;
			  GST_BUFFER_DURATION(buf) = 1;
			  }*/
			GST_BUFFER_FLAGS(buf) = GST_BUFFER_FLAG_LIVE | GST_BUFFER_FLAG_MARKER;
			(GST_OBJECT(c->app_source), GST_BUFFER_PTS(buf));
			//GST_BUFFER_OFFSET(buf) = fcnt++;
			//GST_BUFFER_OFFSET_END(buf) = fcnt;
			gst_app_src_push_buffer((GstAppSrc*)c->app_source, buf);
			bqueue_consume(c->chnl->q);
		}
		else
		{
			fprintf(stderr, "gst audio pipeline did not get data from queue\n");
			struct timespec ts;
			ts.tv_sec = 0;
			ts.tv_nsec = 50 * 10000000;
			nanosleep(&ts, NULL);
		}
			
	}
}

static void feed_jpeg(AppsrcContext* c)
{
	while(!c->busy){
	    const bqueue_entry* e = bqueue_peek(c->chnl->q);
		//LOG_DEBUG("box appsrc[%p] feed %d size=%d", appsrc.p, f->type, f->size);

		if(e != NULL)
		{
			fprintf(stderr, "gst video pipeline got data from queue\n");
			GstBuffer *buf = gst_buffer_new_wrapped(g_memdup((uint8_t*)e->mem, e->length), e->length); 
			//TODO: timestamp it
			/*if(fps){
			  GST_BUFFER_PTS(buf) = (uint64)(fcnt * 1000/fps) * GST_MSECOND;
			  GST_BUFFER_DURATION(buf) = 1000/fps*GST_MSECOND;
			  } else {
			  GST_BUFFER_PTS(buf) = f->timestamp;
			  GST_BUFFER_DURATION(buf) = 1;
			  }*/
			GST_BUFFER_FLAGS(buf) = GST_BUFFER_FLAG_LIVE | GST_BUFFER_FLAG_MARKER;
			(GST_OBJECT(c->app_source), GST_BUFFER_PTS(buf));
			//GST_BUFFER_OFFSET(buf) = fcnt++;
			//GST_BUFFER_OFFSET_END(buf) = fcnt;
			gst_app_src_push_buffer((GstAppSrc*)c->app_source, buf);
			bqueue_consume(c->chnl->q);
		}
		else
		{
			fprintf(stderr, "gst video pipeline did not get data from queue\n");
			struct timespec ts;
			ts.tv_sec = 0;
			ts.tv_nsec = 50 * 10000000;
			nanosleep(&ts, NULL);
		}
	}
}

static void nop(void*p){}
static void need_data(GstAppSrc *src, guint length, gpointer user_data)
{
	fprintf(stderr, "gst audio pipeline need data\n");
	AppsrcContext *c = (AppsrcContext*)user_data;
	c->busy = false;
	//feed(c);
	feed_jpeg(c);
}

static void enough_data(GstAppSrc *src, gpointer user_data)
{
	fprintf(stderr, "gst audio pipeline has enough data\n");
	AppsrcContext* c = (AppsrcContext*)user_data;
	c->busy = true;
}

static gboolean seek_data(GstAppSrc *src, guint64 offset, gpointer user_data)
{
	return 0;
}

/* This function is called when an error message is posted on the bus */
static void error_cb (GstBus *bus, GstMessage *msg, AppsrcContext* data) {
	GError *err;
	gchar *debug_info;

	/* Print error details on the screen */
	gst_message_parse_error (msg, &err, &debug_info);
	g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
	g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
	g_clear_error (&err);
	g_free (debug_info);
}

int pipeline_stop(AppsrcContext* data)
{
	/* Free resources */
	gst_element_set_state (data->pipeline, GST_STATE_NULL);
	gst_object_unref (data->pipeline);
}

int audiopipeline_init(RXCHNL* chnl) {
    AppsrcContext* data;
    AudioContext* audioData;
	GstPad *tee_audio_pad, *tee_video_pad, *tee_app_pad;
	GstPad *queue_audio_pad, *queue_video_pad, *queue_app_pad;
	GstCaps *audio_caps;
	GstBus *bus;

	/* Initialize custom data structure */
	data = (AppsrcContext*)malloc(sizeof(AppsrcContext));
    memset (data, 0, sizeof (data));
	data->chnl = chnl;
    data->busy = false;	
    audioData = (AudioContext*)malloc(sizeof(AudioContext));
    data->audioCtx = audioData;
    /* Initialize GStreamer */
	gst_init (NULL, NULL);
	

	/* Create the elements */
	/*GstElement *pipeline, *app_source, *audio_convert1, *audio_resample, *opus_enc, *rtp_opuspay, *udp_sink;*/
	data->app_source = gst_element_factory_make ("appsrc", "audio_source");
	audioData->audio_convert1 = gst_element_factory_make ("audioconvert", "audio_convert1");
	audioData->audio_resample = gst_element_factory_make ("audioresample", "audio_resample");
	audioData->opus_enc = gst_element_factory_make ("opusenc", "opus_enc");
	audioData->rtp_opuspay = gst_element_factory_make ("rtpopuspay", "rtp_opuspay");
	audioData->udp_sink = gst_element_factory_make ("udpsink", "udp_sink");
    g_object_set(audioData->rtp_opuspay, "encoding-name", "OPUS", NULL);	
    g_object_set(audioData->udp_sink, "host", "192.168.1.31", NULL);

	/* Create the empty pipeline */
	data->pipeline = gst_pipeline_new ("test-pipeline");

	if (!data->pipeline || !data->app_source || !audioData->audio_convert1 ||
			!audioData->audio_resample || !audioData->opus_enc || !audioData->rtp_opuspay || !audioData->udp_sink ) {
		g_printerr ("Not all elements could be created.\n");
		return -1;
	}

	/* Configure appsrc */
	gst_audio_info_set_format (&audioInfo, GST_AUDIO_FORMAT_S16, SAMPLE_RATE, 1, NULL);
	audio_caps = gst_audio_info_to_caps (&audioInfo);
	g_object_set (data->app_source, "caps", audio_caps, "format", GST_FORMAT_TIME, NULL);
	srccbs.need_data = need_data;
	srccbs.enough_data = enough_data;
	srccbs.seek_data = seek_data;
	gst_app_src_set_callbacks((GstAppSrc*)data->app_source, &srccbs, data, nop);

	//TODO: configure udpsink, host and port, default is localhost and 5004
	/* Link all elements that can be automatically linked because they have "Always" pads */
	gst_bin_add_many (GST_BIN (data->pipeline), data->app_source, audioData->audio_convert1, audioData->audio_resample, audioData->opus_enc, audioData->rtp_opuspay, audioData->udp_sink, NULL);
	if (gst_element_link_many (data->app_source, audioData->audio_convert1, audioData->audio_resample, audioData->opus_enc, audioData->rtp_opuspay, audioData->udp_sink, NULL) != TRUE ) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data->pipeline);
		return -1;
	}

	/* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
	bus = gst_element_get_bus (data->pipeline);
	gst_bus_add_signal_watch (bus);
	g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)error_cb, data);
	gst_object_unref (bus);

	/* Start playing the pipeline */
	gst_element_set_state (data->pipeline, GST_STATE_PLAYING);
	fprintf(stderr, "gst audio pipeline of opusenc and rtpopus pay initialized\n");

	/* Create a GLib Main Loop and set it to run */
	//data.main_loop = g_main_loop_new (NULL, FALSE);
	//g_main_loop_run (data.main_loop);

	return 0;
}

int videopipeline_init(RXCHNL* chnl) {
    AppsrcContext* data; 
    VideoContext* videoData;
	GstPad *tee_audio_pad, *tee_video_pad, *tee_app_pad;
	GstPad *queue_audio_pad, *queue_video_pad, *queue_app_pad;
	GstCaps *img_caps;
	GstCaps *video_caps;
	GstBus *bus;

	/* Initialize cumstom data structure */
	data = (AppsrcContext*)malloc(sizeof(AppsrcContext));
    memset (data, 0, sizeof (data));
	data->chnl = chnl;
    data->busy = false;	
    videoData = (VideoContext*)malloc(sizeof(VideoContext));
    data->videoCtx = videoData;
    /* Initialize GStreamer */
	gst_init (NULL, NULL);
	
	/* Create the elements: caps="image/jpeg, framerate=(fraction)5/1" ! nvjpegdec ! "video/x-raw(memory:NVMM), format=(string)I420, width=2048, height=1568" ! omxh264enc iframeinterval=30 insert-sps-pps=true profile=1  ! rtph264pay ! udpsink host=192.168.1.31 port=9000 sync=false*/
	data->app_source = gst_element_factory_make ("appsrc", "video_source");
    videoData->jpeg_dec = gst_element_factory_make("nvjpegdec", "jpeg_dec");
    videoData->h264_enc = gst_element_factory_make ("omxh264enc", "omxh264_enc");
	videoData->rtp_h264pay = gst_element_factory_make ("rtph264pay", "rtp_h264pay");
	videoData->udp_sink = gst_element_factory_make ("udpsink", "udp_sink");
    g_object_set(videoData->h264_enc, "iframeinterval", 10, "insert-sps-pps", true, "profile", 1, NULL);	
    g_object_set(videoData->rtp_h264pay, "encoding-name", "H264", NULL);	
    g_object_set(videoData->udp_sink, "host", "192.168.1.31", NULL);

	/* Create the empty pipeline */
	data->pipeline = gst_pipeline_new ("test-pipeline");

	if (!data->pipeline || !data->app_source || !videoData->jpeg_dec || !videoData->h264_enc ||
			!videoData->rtp_h264pay || !videoData->udp_sink ) {
		g_printerr ("Not all elements could be created.\n");
		return -1;
	}

	/* Configure appsrc */
    img_caps = gst_caps_from_string("image/jpeg");
    g_object_set (data->app_source, "caps", img_caps, NULL);
	srccbs.need_data = need_data;
	srccbs.enough_data = enough_data;
	srccbs.seek_data = seek_data;
	gst_app_src_set_callbacks((GstAppSrc*)data->app_source, &srccbs, data, nop);
    video_caps = gst_caps_from_string("video/x-raw, format=(string)I420, width=2048, height=1568");
	//TODO: configure udpsink, host and port, default is localhost and 5004
	/* Link all elements that can be automatically linked because they have "Always" pads */
	gst_bin_add_many (GST_BIN (data->pipeline), data->app_source, videoData->jpeg_dec, videoData->h264_enc, videoData->rtp_h264pay, videoData->udp_sink, NULL);
	if(gst_element_link_filtered(videoData->jpeg_dec, videoData->h264_enc, video_caps) != TRUE) {
        g_printerr("link jpeg_dec to h264_enc error. \n");
        gst_object_unref(data->pipeline);
        return -1;
    }
    
    if(gst_element_link(data->app_source, videoData->jpeg_dec) != TRUE){
        g_printerr("link appsrc to jpeg_dec rror \n");
        gst_object_unref(data->pipeline);
        return -1;
    }
    
    if (gst_element_link_many (videoData->h264_enc, videoData->rtp_h264pay, videoData->udp_sink, NULL) != TRUE ) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data->pipeline);
		return -1;
	}

	/* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
	bus = gst_element_get_bus (data->pipeline);
	gst_bus_add_signal_watch (bus);
	g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)error_cb, data);
	gst_object_unref (bus);

	/* Start playing the pipeline */
	gst_element_set_state (data->pipeline, GST_STATE_PLAYING);
	fprintf(stderr, "gst video pipeline of nvjpegdec, omxh264enc and rtph264pay initialized\n");

	return 0;
}

//~/tjdata/tjtools/tjcalib/imgs/calibration/cam0/bak/camera_?.jpg
// gst-launch-1.0 multifilesrc location=./camera_%d.jpg start-index=0 stop-index=58 loop=true caps="image/jpeg, framerate=(fraction)5/1" ! nvjpegdec ! nvoverlaysink
// gst-launch-1.0 multifilesrc location=./camera_%d.jpg start-index=0 stop-index=58 loop=true caps="image/jpeg, framerate=(fraction)15/1" ! nvjpegdec ! "video/x-raw, format=(string)I420, width=1052, height=780" ! omxh264enc iframeinterval=30 insert-sps-pps=true profile=1  ! rtph264pay ! udpsink host=192.168.1.33 port=9000 sync=false
// receiver: gst-launch-1.0 udpsrc port=9000 ! application/x-rtp,payload=96 ! rtph264depay ! decodebine ! autovideosink sync=false
//
/*
receive audio:
    export AUDIO_CAPS="application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00"
    gst-launch-1.0 udpsrc caps=$AUDIO_CAPS ! rtpopusdepay ! opusdec ! autoaudiosink
send audio: fc2ctrl -rx 1:-

receive video:
gst-launch-1.0 udpsrc port=5004 caps="application/x-rtp, media=(string)video, clock-rat=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! h264parse ! openh264dec ! autovideosink sync=false
send video: fc2ctrl -rx 8:-
*/
