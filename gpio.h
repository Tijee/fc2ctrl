#ifndef __gpio_h
#define __gpio_h

#ifdef PLATFORM_PI

#else
    
#endif

typedef enum {
    GPIOMODE_INPUT,
    GPIOMODE_OUTPUT,
    GPIOMODE_INPUT_PULLUP,
    GPIOMODE_INPUT_PULLDOWN,
    GPIOMODE_OPEN_DRAIN,
    GPIOMODE_OPEN_DRAIN_PULLUP
} GPIOMODE;

bool gpio_init();
void gpio_mode(uint8_t idx, GPIOMODE mode);
bool gpio_read(uint8_t idx);
void gpio_set(uint8_t idx);
void gpio_clr(uint8_t idx);

bool gpio_jtag_init();
void gpio_jtag_quit(void);

bool gpio_jtag_read(void);
void gpio_jtag_write(bool tck, bool tms, bool tdi);

void gpio_jtag_xfer(int n, const uint8_t* ptms, const uint8_t* ptdi, uint8_t* ptdo)
#ifdef gpio_jtag_xfer_imp
{
    //printf("gpio_jtag_xfer2 %d %p %p %p\n", n, ptms, ptdi, ptdo);
	while(n>0){
        uint8_t tms = *ptms++;
        uint8_t tdi = *ptdi++;
        uint8_t tdo = 0;
        for(int i=0; i<8 && i<n; i++){
            gpio_jtag_write(0, tms & 1, tdi & 1);
            tdo |= gpio_jtag_read() << i;
            gpio_jtag_write(1, tms & 1, tdi & 1);
            tms >>= 1;
            tdi >>= 1;
        }
        *ptdo++ = tdo;
        n -= 8;
	}
}
#else
;
#endif

int fpgamode(int mode);

#endif
