#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>

#include <sys/mman.h>
#include <fcntl.h>

#define gpio_jtag_xfer_imp
#include "gpio.h"

#define BCM2835_PERI_BASE   0x3f000000
#define BCM2835_GPIO_BASE	(BCM2835_PERI_BASE + 0x200000) /* GPIO controller */

#define BCM2835_PADS_GPIO_0_27		(BCM2835_PERI_BASE + 0x100000)
#define BCM2835_PADS_GPIO_0_27_OFFSET	(0x2c / 4)

/* GPIO setup macros */
#define MODE_GPIO(g) (*(pio_base+((g)/10))>>(((g)%10)*3) & 7)
#define INP_GPIO(g) do { *(pio_base+((g)/10)) &= ~(7<<(((g)%10)*3)); } while (0)
#define SET_MODE_GPIO(g, m) do { /* clear the mode bits first, then set as necessary */ \
		INP_GPIO(g);						\
		*(pio_base+((g)/10)) |=  ((m)<<(((g)%10)*3)); } while (0)
#define OUT_GPIO(g) SET_MODE_GPIO(g, 1)

#define GPIO_SET (*(pio_base+7))  /* sets   bits which are 1, ignores bits which are 0 */
#define GPIO_CLR (*(pio_base+10)) /* clears bits which are 1, ignores bits which are 0 */
#define GPIO_LEV (*(pio_base+13)) /* current level of the pin */

static int dev_mem_fd;
static volatile uint32_t *pio_base;

bool gpio_read(uint8_t idx)
{
    return (GPIO_LEV & (1<<idx)) > 0;
}

void gpio_set(uint8_t idx)
{
    GPIO_SET = 1<<idx;
}

void gpio_clr(uint8_t idx)
{
    GPIO_CLR = 1<<idx;
}

void gpio_mode(uint8_t idx, GPIOMODE mode)
{
    SET_MODE_GPIO(idx, (uint32_t)mode);
}


#define GPIO_PWREN 26
#define GPIO_ARESETN 25
#define GPIO_TCK 18
#define GPIO_TMS 22
#define GPIO_TDI 23
#define GPIO_TDO 24


int fpgamode(int mode)
{
    if( !gpio_init() ) return 2;
    
    gpio_mode(GPIO_PWREN, GPIOMODE_OUTPUT);
    
    if(mode == 1){
        gpio_clr(GPIO_PWREN);
        sleep(1);
        gpio_set(GPIO_PWREN);
        return 0;
    }

    if(mode == 2){
        gpio_set(GPIO_PWREN);
        return 0;
    }
    
    if(mode == 3){
        gpio_clr(GPIO_PWREN);
        return 0;
    }
    
    if(mode == 4){
        gpio_mode(GPIO_ARESETN, GPIOMODE_OUTPUT);
        gpio_clr(GPIO_ARESETN);
        gpio_mode(GPIO_ARESETN, GPIOMODE_INPUT);
        return 0;
    }
    return 1;        
}



/* GPIO numbers for each signal. Negative values are invalid */
static uint32_t tck_gpio = 1<<GPIO_TCK;
static uint32_t tms_gpio = 1<<GPIO_TMS;
static uint32_t tdi_gpio = 1<<GPIO_TDI;
static uint32_t tdo_gpio = 1<<GPIO_TDO;


bool gpio_jtag_read(void)
{
	return !!(GPIO_LEV & tdo_gpio);
}

void gpio_jtag_write(bool tck, bool tms, bool tdi)
{
	uint32_t set = (tck ? tck_gpio : 0) | (tms ? tms_gpio : 0) | (tdi ? tdi_gpio : 0);
	uint32_t clear = (~set) & (tck_gpio | tms_gpio | tdi_gpio);
	GPIO_SET = set;
	GPIO_CLR = clear;
    // delay
    GPIO_SET = 0;
    GPIO_SET = 0;
    GPIO_SET = 0;
}

bool gpio_init()
{
	dev_mem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (dev_mem_fd < 0) {
		perror("open");
		return false;
	}

	pio_base = mmap(NULL, sysconf(_SC_PAGE_SIZE), PROT_READ | PROT_WRITE,
				MAP_SHARED, dev_mem_fd, BCM2835_GPIO_BASE);

	if (pio_base == MAP_FAILED) {
		perror("mmap");
		close(dev_mem_fd);
		return false;
	}

	static volatile uint32_t *pads_base;
	pads_base = mmap(NULL, sysconf(_SC_PAGE_SIZE), PROT_READ | PROT_WRITE,
				MAP_SHARED, dev_mem_fd, BCM2835_PADS_GPIO_0_27);

	if (pads_base == MAP_FAILED) {
		perror("mmap");
		close(dev_mem_fd);
		return false;
	}

	/* set 4mA drive strength, slew rate limited, hysteresis on */
	pads_base[BCM2835_PADS_GPIO_0_27_OFFSET] = 0x5a000008 + 1;
    return true;
}

bool gpio_jtag_init()
{
    if(!pio_base) return false;
    
	/*
	 * Configure TDO as an input, and TDI, TCK, TMS
	 * as outputs.  Drive TDI and TCK low, and TMS high.
	 */
	INP_GPIO(GPIO_TDO);

	GPIO_CLR = tdi_gpio | tck_gpio;
	GPIO_SET = tms_gpio;

	OUT_GPIO(GPIO_TDI);
	OUT_GPIO(GPIO_TCK);
	OUT_GPIO(GPIO_TMS);

	gpio_jtag_write(0, 1, 0);

	return true;
}

void gpio_jtag_quit()
{
	INP_GPIO(GPIO_TDI);
	INP_GPIO(GPIO_TCK);
	INP_GPIO(GPIO_TMS);
}
