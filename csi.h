#ifndef  CSI_H
#define  CSI_H

#include "queue.h"
#include "sdecode.h"

struct _RXCHNL;
struct _RXCFG;

typedef void (*chnlrecv)(struct _RXCHNL*chnl, const uint32_t*data, int size, int flags);
typedef void (*chnlrun)(struct _RXCHNL*chnl);

typedef struct _RXCHNL {
    int id, fd, type, dcnt, odcnt;
    const uint32_t* info;
    chnlrecv recv;
    chnlrun run;
    bqueue* q;
    int vars[4];
} RXCHNL;


extern int seconds, vsize, vline, vtype, dline, framecnt;
extern uint32_t rxtiming[9];

void csisetsize(int w, int h);
int csirun(int w, int h);


typedef struct tOBUF{
    struct tOBUF* next;
    struct tOBUF* priv;
    uint32_t cmd;
    uint8_t *data;
    uint32_t alloc_size;
    uint32_t length;
} OBUF;

OBUF* obufget();
void obufsend(OBUF* p);
void obufrelease(OBUF* p);


#endif
