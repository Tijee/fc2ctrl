#ifndef sdecode_h
#define sdecode_h

typedef struct _MSGHDR {
    unsigned idx: 12;  // last byte idx
    unsigned bid: 4;   // packet id in group, if id>=8 then means correction packet
    unsigned chnl: 4;  // channel id
    unsigned flags: 2; // NONE=0, SOF=2, EOF=1
    unsigned gid: 2;   // group id
    unsigned ecc: 6;   // ecc
    unsigned xflag :2; // in air: = 3.  in MSGCHNL: !=0 original data, ==0 copied data
    unsigned data[1];  // data. checksum is after data
} MSGHDR;


typedef struct _MSGCHNL {
    const MSGHDR* msg[8];
    MSGHDR*  mdata[8];   // malloc data
    unsigned* vchecksum; // temp data for checksum, mdcnt
    unsigned mdcnt;      // max dwcount of data in MSGHDR. so actual malloc size is (mdcnt+2)*4
    unsigned gid;
    unsigned gcnt;
    unsigned glost;
    unsigned ext[1];     // extcnt
} MSGCHNL;


#ifdef SDECODE_IMP

MSGCHNL* chnls[16];
unsigned scntbytes, scntebits;

void scput(MSGCHNL* chnl, const MSGHDR* msg);
unsigned scinit(unsigned id, MSGCHNL* chnl);   // return MDCNT
void scquit(MSGCHNL* chnl);

uint8_t ecc24(const uint8_t* hdr)
{
    static const uint8_t ecc24_0[] = {
      0,   7,  11,  12,  13,  10,   6,   1, 
     14,   9,   5,   2,   3,   4,   8,  15, 
     19,  20,  24,  31,  30,  25,  21,  18, 
     29,  26,  22,  17,  16,  23,  27,  28, 
     21,  18,  30,  25,  24,  31,  19,  20, 
     27,  28,  16,  23,  22,  17,  29,  26, 
      6,   1,  13,  10,  11,  12,   0,   7, 
      8,  15,   3,   4,   5,   2,  14,   9, 
     22,  17,  29,  26,  27,  28,  16,  23, 
     24,  31,  19,  20,  21,  18,  30,  25, 
      5,   2,  14,   9,   8,  15,   3,   4, 
     11,  12,   0,   7,   6,   1,  13,  10, 
      3,   4,   8,  15,  14,   9,   5,   2, 
     13,  10,   6,   1,   0,   7,  11,  12, 
     16,  23,  27,  28,  29,  26,  22,  17, 
     30,  25,  21,  18,  19,  20,  24,  31, 
     25,  30,  18,  21,  20,  19,  31,  24, 
     23,  16,  28,  27,  26,  29,  17,  22, 
     10,  13,   1,   6,   7,   0,  12,  11, 
      4,   3,  15,   8,   9,  14,   2,   5, 
     12,  11,   7,   0,   1,   6,  10,  13, 
      2,   5,   9,  14,  15,   8,   4,   3, 
     31,  24,  20,  19,  18,  21,  25,  30, 
     17,  22,  26,  29,  28,  27,  23,  16, 
     15,   8,   4,   3,   2,   5,   9,  14, 
      1,   6,  10,  13,  12,  11,   7,   0, 
     28,  27,  23,  16,  17,  22,  26,  29, 
     18,  21,  25,  30,  31,  24,  20,  19, 
     26,  29,  17,  22,  23,  16,  28,  27, 
     20,  19,  31,  24,  25,  30,  18,  21, 
      9,  14,   2,   5,   4,   3,  15,   8, 
      7,   0,  12,  11,  10,  13,   1,   6 
    };
    static const uint8_t ecc24_1[] = {
      0,  26,  28,   6,  35,  57,  63,  37, 
     37,  63,  57,  35,   6,  28,  26,   0, 
     38,  60,  58,  32,   5,  31,  25,   3, 
      3,  25,  31,   5,  32,  58,  60,  38, 
     41,  51,  53,  47,  10,  16,  22,  12, 
     12,  22,  16,  10,  47,  53,  51,  41, 
     15,  21,  19,   9,  44,  54,  48,  42, 
     42,  48,  54,  44,   9,  19,  21,  15, 
     42,  48,  54,  44,   9,  19,  21,  15, 
     15,  21,  19,   9,  44,  54,  48,  42, 
     12,  22,  16,  10,  47,  53,  51,  41, 
     41,  51,  53,  47,  10,  16,  22,  12, 
      3,  25,  31,   5,  32,  58,  60,  38, 
     38,  60,  58,  32,   5,  31,  25,   3, 
     37,  63,  57,  35,   6,  28,  26,   0, 
      0,  26,  28,   6,  35,  57,  63,  37, 
     44,  54,  48,  42,  15,  21,  19,   9, 
      9,  19,  21,  15,  42,  48,  54,  44, 
     10,  16,  22,  12,  41,  51,  53,  47, 
     47,  53,  51,  41,  12,  22,  16,  10, 
      5,  31,  25,   3,  38,  60,  58,  32, 
     32,  58,  60,  38,   3,  25,  31,   5, 
     35,  57,  63,  37,   0,  26,  28,   6, 
      6,  28,  26,   0,  37,  63,  57,  35, 
      6,  28,  26,   0,  37,  63,  57,  35, 
     35,  57,  63,  37,   0,  26,  28,   6, 
     32,  58,  60,  38,   3,  25,  31,   5, 
      5,  31,  25,   3,  38,  60,  58,  32, 
     47,  53,  51,  41,  12,  22,  16,  10, 
     10,  16,  22,  12,  41,  51,  53,  47, 
      9,  19,  21,  15,  42,  48,  54,  44, 
     44,  54,  48,  42,  15,  21,  19,   9 
    };
    static const uint8_t ecc24_2[] = {
      0,  49,  50,   3,  52,   5,   6,  55, 
     56,   9,  10,  59,  12,  61,  62,  15, 
     31,  46,  45,  28,  43,  26,  25,  40, 
     39,  22,  21,  36,  19,  34,  33,  16, 
     47,  30,  29,  44,  27,  42,  41,  24, 
     23,  38,  37,  20,  35,  18,  17,  32, 
     48,   1,   2,  51,   4,  53,  54,   7, 
      8,  57,  58,  11,  60,  13,  14,  63, 
     55,   6,   5,  52,   3,  50,  49,   0, 
     15,  62,  61,  12,  59,  10,   9,  56, 
     40,  25,  26,  43,  28,  45,  46,  31, 
     16,  33,  34,  19,  36,  21,  22,  39, 
     24,  41,  42,  27,  44,  29,  30,  47, 
     32,  17,  18,  35,  20,  37,  38,  23, 
      7,  54,  53,   4,  51,   2,   1,  48, 
     63,  14,  13,  60,  11,  58,  57,   8, 
     59,  10,   9,  56,  15,  62,  61,  12, 
      3,  50,  49,   0,  55,   6,   5,  52, 
     36,  21,  22,  39,  16,  33,  34,  19, 
     28,  45,  46,  31,  40,  25,  26,  43, 
     20,  37,  38,  23,  32,  17,  18,  35, 
     44,  29,  30,  47,  24,  41,  42,  27, 
     11,  58,  57,   8,  63,  14,  13,  60, 
     51,   2,   1,  48,   7,  54,  53,   4, 
     12,  61,  62,  15,  56,   9,  10,  59, 
     52,   5,   6,  55,   0,  49,  50,   3, 
     19,  34,  33,  16,  39,  22,  21,  36, 
     43,  26,  25,  40,  31,  46,  45,  28, 
     35,  18,  17,  32,  23,  38,  37,  20, 
     27,  42,  41,  24,  47,  30,  29,  44, 
     60,  13,  14,  63,   8,  57,  58,  11, 
      4,  53,  54,   7,  48,   1,   2,  51 
    };
    return ecc24_0[hdr[0]] ^ ecc24_1[hdr[1]] ^ ecc24_2[hdr[2]];
}

uint8_t ecc24c(const uint8_t* hdr)
{
    static const uint8_t ecc24_t[] = {
    30, 30, 30, 31, 30, 31, 31,  0, 
    30, 31, 31,  1, 31,  2,  3, 31, 
    30, 31, 31,  4, 31,  5,  6, 31, 
    31,  7,  8, 31,  9, 31, 31, 20, 
    30, 31, 31, 10, 31, 11, 12, 31, 
    31, 13, 14, 31, 15, 31, 31, 21, 
    31, 16, 17, 31, 18, 31, 31, 22, 
    19, 31, 31, 23, 31, 31, 31, 31 
    };
    uint8_t v = ecc24(hdr) ^ (hdr[3]&0x3f);
    return ecc24_t[v];
}

const uint8_t* dbase;

#define SDECODE_ERR

void scdata(const MSGHDR* msg, const unsigned* data)
{
    MSGCHNL* chnl = chnls[msg->chnl];
    if(!chnl || chnl->mdcnt == 0)return;
    
    scntbytes += msg->idx + 1;
    
    uint8_t gid = msg->gid;
    uint8_t bid = msg->bid;
    
    if(chnl->gid == 8) chnl->gid = gid;
    
    // checksum msg is lost. commit
    if(chnl->gcnt && chnl->gid != gid){
        bool samesize = false;
        for(unsigned i=0; i<8; i++){
            if(chnl->msg[i] && chnl->msg[i]->idx == msg->idx) samesize = true;
        }
        if(samesize && !chnl->glost){
#ifdef SDECODE_ERR
            fprintf(stderr, "chnl[%u] gid %d.%d -> %d.%d @%lx\n", msg->chnl, gid, bid, chnl->gid, chnl->gcnt, (uint8_t*)data - dbase);
            chnl->glost ++;
            return;
#endif        
        } else {
            for(unsigned i=0; i<8; i++){
                if(chnl->msg[i]) scput(chnl, chnl->msg[i]);
                chnl->msg[i] = NULL;
            }
            
#ifdef SDECODE_ERR
            fprintf(stderr, "chnl[%u] gid %d.%d->%d.%d @%lx\n", msg->chnl, chnl->gid, chnl->gcnt, gid, bid, (uint8_t*)data - dbase);
#endif        
            chnl->gcnt = 0;
            chnl->gid = gid;
        }
    }
    
    // data msg
    if(bid < 8){
        if(msg->idx/4+1 > chnl->mdcnt) return;  // bad msg size
        if(chnl->msg[bid]) return; // duplicated msg!
        if(msg->xflag == 0){ // memcpy is needed for header changed 
            chnl->msg[bid] = chnl->mdata[bid];
            *chnl->mdata[bid] = *msg;  // header & data[0]
            memcpy(&chnl->mdata[bid]->data[1], &data[1], (msg->idx/4+1)*4); // data[1..] & checksum
        } else {
            chnl->msg[bid] = msg;
        }
        chnl->gcnt += 1;
        return;
    }
 
    // all data + checksum here, check them and commit
    if(chnl->gcnt + 7 == bid){
        unsigned* vchecksum = NULL;
        for(unsigned n=0; n<chnl->gcnt; n++)if(chnl->msg[n]){
            const MSGHDR* hdr = chnl->msg[n]; 
            unsigned checksum = hdr->data[0];
            for(unsigned i=1; i<hdr->idx/4+2; i++){
                checksum ^= hdr->data[i];
            }
            if(checksum == 0) {
                scput(chnl, hdr);
                continue;
            }
            
#ifdef SDECODE_ERR
            fprintf(stderr, "chnl[%u] cerr=%x @%lx\n", msg->chnl, checksum, (uint8_t*)data - dbase);
#endif            
            
            // recover bit error
            if(!vchecksum){
                // cal all vchecksum
                vchecksum = chnl->vchecksum;
                for(unsigned i=0; i<msg->idx/4+1; i++){
                    vchecksum[i] = data[i];
                    for(unsigned k=0; k<chnl->gcnt; k++){
                        vchecksum[i] ^= chnl->msg[k]->data[i];
                    }
                    if(vchecksum[i])scntebits += 1;
                }
            }
            MSGHDR* nmsg = chnl->mdata[n];
            if(nmsg != hdr) *nmsg = *hdr;
            for(unsigned i=0; i<nmsg->idx/4+1; i++){
                nmsg->data[i] = hdr->data[i] ^ (vchecksum[i] & checksum);
            }

            scput(chnl, nmsg);
        }
    }
    
    // lost 1 data msg, recover data and commit. problem is that flag & idx are lost too.
    if(chnl->gcnt + 8 == bid){
        scntebits += 64;
        
        
        for(unsigned n=0; n<chnl->gcnt; n++)if(chnl->msg[n]){
            scput(chnl, chnl->msg[n]);
        } else {
#ifdef SDECODE_ERR
            fprintf(stderr, "chnl[%u] lerr %u @%lx\n", msg->chnl, n, (uint8_t*)data - dbase);
#endif            
            
            MSGHDR*nmsg = chnl->mdata[n];
            *nmsg = *msg;
            for(unsigned i=0; i<msg->idx/4+1; i++){
                nmsg->data[i] = data[i];
                for(unsigned j=0; j<8; j++)if(chnl->msg[j]){
                    nmsg->data[i] ^= chnl->msg[j]->data[i];
                }
            }
            scput(chnl, nmsg);
        }
    }
    
    // finish or lost too many msgs
    for(int i=0; i<8; i++)chnl->msg[i] = NULL;
    chnl->gcnt = 0;
    chnl->gid = 8;
    chnl->glost = 0;
}


void sdecode(const uint8_t * msg, unsigned width, unsigned size)
{
    bool eof = false;
    dbase = msg;
    for(int n = 0; n < size; n += width){
        static uint32_t tsize = 0;
        const uint32_t* msge = (const uint32_t*)&msg[n+width-16];
        
        const MSGHDR* hdr = (const MSGHDR*)&msg[n];
        uint32_t hdrv = *(const uint32_t*)hdr;
        if((hdrv>>16) == 0x001e){
            static uint16_t next;
            hdrv &= 0xffff;
            if(next != hdrv && ( ((next+1)&0xffff) != hdrv || n>0) ){
                if(eof)break;
                scntebits += width*8;
#ifdef SDECODE_ERR
                fprintf(stderr, "L(%x->%x@%d,%x) ", next, hdrv, n/width, tsize);
#endif
            }
            next = (hdrv+1)&0xffff;
            hdr = (const MSGHDR*)&hdr->data[0];
        }
        tsize += width;
        if(eof)continue;
        while((const uint32_t*)hdr < msge){
            if(hdr->xflag == 0){
                hdr = (const MSGHDR*)&hdr->data[0];
                continue;
            }
            if(*(const uint32_t*)hdr == 0xffffffff){
                eof = 1;
                break;
            }
            
            uint8_t b = ecc24c((const uint8_t*)hdr);
            if(b == 31){
#ifdef SDECODE_ERR                
                fprintf(stderr, "DERR v=%x @%d.%ld\n", *(unsigned*)hdr, n/width, (uint32_t*)hdr - (uint32_t*)&msg[n]);
#endif                
                break;
            } else if(b == 30) {
                scdata(hdr, hdr->data);
                hdr = (const MSGHDR*)&hdr->data[hdr->idx/4+2]; // skip data & checksum
                
            } else if(b < 24) {
                MSGHDR t = *hdr;
                *(unsigned*)&t ^= 1<<b;
                t.xflag = 0;
                scdata(&t, hdr->data);
                
#ifdef SDECODE_ERR                
                fprintf(stderr, "ECC v=%x b=%d idx=%d @%d.%ld o=%x\n", *(unsigned*)hdr, b, t.idx, n/width, (uint32_t*)hdr - (uint32_t*)&msg[n], *(uint32_t*)&msg[n]);
#endif                
                
                hdr = (const MSGHDR*)&hdr->data[t.idx/4+2]; // skip data & checksum
                scntebits += 1;
            }
            
            //printf("V=%x @%p\n", *(unsigned*)hdr, hdr);
        }
    }
}


bool sinit(unsigned extcnt)
{
    MSGCHNL* chnl = NULL;
    for(unsigned n=0; n<16; n++){
        if(!chnl){
            chnl = (MSGCHNL*)malloc(sizeof(MSGCHNL) + extcnt * sizeof(unsigned));
            if(!chnl)return false;
            memset(chnl, 0, sizeof(MSGCHNL) + extcnt * sizeof(unsigned));
        }
        chnl->mdcnt = scinit(n, chnl);
        if(chnl->mdcnt == 0) continue;
        chnls[n] = chnl;
        
        chnl->vchecksum = (unsigned*)malloc( (chnl->mdcnt+2)*sizeof(unsigned)*9 );
        if(!chnl->vchecksum) return false;
        
        for(unsigned j=0; j<8; j++){
            chnl->mdata[j] = (MSGHDR*)&chnl->vchecksum[chnl->mdcnt + (chnl->mdcnt+2)*j];
        }
        chnl = NULL;
    }
    if(chnl)free(chnl);
    return true;
}

void squit()
{
    for(unsigned n=0; n<16; n++)if(chnls[n]) {
        scquit(chnls[n]);
        free(chnls[n]->vchecksum);
        free(chnls[n]);
        chnls[n] = NULL;
    }
}

#else
extern MSGCHNL* chnls[16];
extern unsigned scntbytes, scntebits;

bool sinit(unsigned extcnt);
void sdecode(const uint8_t * msg, unsigned width, unsigned size);
void squit();
#endif

#endif

