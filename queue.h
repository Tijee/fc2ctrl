#ifndef _queue_h
#define _queue_h

#include <stdatomic.h>

typedef struct _queue {
    _Atomic int wptr, rptr;
    int size;
    uint8_t mem[1];
} queue;

static queue* queue_init(int size)
{
    queue* q = (queue*)malloc(size+16);
    if(q == NULL) return NULL;
    atomic_init(&q->wptr, 0);
    atomic_init(&q->rptr, 0);
    q->size = size;
    for(int i=0; i<size; i+=4096) q->mem[i] = 0;
    return q;
}

static void queue_free(queue* q)
{
    free(q);
}

static int queue_produce(queue* q, const uint8_t* mem, int size)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    int rptr = atomic_load_explicit(&q->rptr, memory_order_acquire);
    
    int ptr = 0;
    if(wptr >= rptr){
        int csz = q->size - wptr;
        if(csz > size) csz = size;
        memcpy(&q->mem[wptr], mem, csz);
        ptr += csz;
        mem += csz;
        wptr = (wptr + csz) % q->size;
    }
    
    if(rptr > wptr){
        int csz = rptr - wptr - 1;
        if(csz > size-ptr) csz = size-ptr;
        memcpy(&q->mem[wptr], &mem[ptr], csz);
        ptr += csz;
        wptr += csz;
    }
    
    atomic_store_explicit(&q->wptr, wptr, memory_order_release);
    return ptr;
}

static const uint8_t* queue_peek(queue* q, int* size)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    int rptr = atomic_load_explicit(&q->rptr, memory_order_acquire);
    
    if(wptr > rptr){
        *size = wptr - rptr;
        return &q->mem[rptr];
    }
    if(wptr < rptr){
        *size = q->size - rptr;
        return &q->mem[rptr];
    }
    return NULL;
}

static void queue_consume(queue* q, int size)
{
    atomic_store_explicit(&q->rptr, (q->rptr+size)%q->size, memory_order_release);
}


typedef struct _bqueue_entry {
    int length;
    uint8_t mem[1];
} bqueue_entry;

typedef struct _bqueue {
    _Atomic int wptr, rptr;
    int bsize, bcount;
    bqueue_entry* data[1];
} bqueue;


static bqueue* bqueue_init(int bsize, int bcount)
{
    bqueue* q = (bqueue*)malloc(16 + bcount*sizeof(bqueue_entry*));
    if(q == NULL) return NULL;
    atomic_init(&q->wptr, 0);
    atomic_init(&q->rptr, 0);
    q->bsize = bsize;
    q->bcount = bcount;
    for(int i=0; i<bcount; i++){
        q->data[i] = (bqueue_entry*)malloc(bsize + sizeof(int));
        if(q->data[i] == NULL) return NULL;
        q->data[i]->length = 0;
    }
    return q;
}

static void bqueue_free(bqueue* q)
{
    for(int i=0; i<q->bcount; i++){
        free(q->data[i]);
        q->data[i] = NULL;
    }
    free(q);
}

static void bqueue_setlength(bqueue* q, int len)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    q->data[wptr]->length = len;
}

static int bqueue_append(bqueue* q, const uint8_t* mem, int size)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    bqueue_entry*e = q->data[wptr];
    if(size > q->bsize - e->length) size = q->bsize - e->length;
    memcpy(&e->mem[e->length], mem, size);
    e->length += size;
    return size;
}

static const bqueue_entry* bqueue_current(bqueue* q)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    return q->data[wptr];
}


static int bqueue_produce(bqueue* q)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    int rptr = atomic_load_explicit(&q->rptr, memory_order_acquire);
    if(q->data[wptr]->length == 0)return 1;
    
    wptr = (wptr + 1) % q->bcount;
    if(wptr == rptr) return 0;
    
    atomic_store_explicit(&q->wptr, wptr, memory_order_release);
    return 1;
}

static int bqueue_count(bqueue* q)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    int rptr = atomic_load_explicit(&q->rptr, memory_order_acquire);
    return (wptr + q->bcount - rptr) % q->bcount;
}


static const bqueue_entry* bqueue_peek(bqueue* q)
{
    int wptr = atomic_load_explicit(&q->wptr, memory_order_acquire);
    int rptr = atomic_load_explicit(&q->rptr, memory_order_acquire);
    
    if(rptr == wptr) return NULL;
    return q->data[rptr];
}

static void bqueue_consume(bqueue* q)
{
    q->data[q->rptr]->length = 0;
    atomic_store_explicit(&q->rptr, (q->rptr+1)%q->bcount, memory_order_release);
}


#endif
