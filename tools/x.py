#!/usr/bin/python

import sys, io, os, time, struct
from subprocess import call

outputname = "raw.30s.txt"
with open(outputname, "w") as outfile:
    for n in range(30):
        inputname = "rg10.raw." + str(n)
        call(["xxd", "-c", "48", "-l", "48", inputname], stdout=outfile) 

