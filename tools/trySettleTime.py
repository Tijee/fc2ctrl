#!/usr/bin/python

import sys, io, os, time, struct
from subprocess import call

#clk can be from 0 to 63
clkStart = 0 
clkEnd = 63 
#ths can be from 0 to 63
thsStart = 3
thsEnd = 7 
ths = thsStart 
clk = clkStart 
while clk < clkEnd:
    while ths < thsEnd:
        print("ths:" + str(ths).zfill(2) + ":clk:" + str(clk).zfill(2), file=sys.stdout, flush=True)
        with open('/proc/csiparams', 'w') as procfs:
            csisettletimes = str(ths).zfill(2)+str(clk).zfill(2)
            procfs.write(csisettletimes)
        call(["/home/nvidia/yavta/yavta", "-f", "SRGGB8", "-c20", "-s", "3456x2580", "/dev/video0"])
        ths += 1
    clk +=1
    ths = thsStart;

