#!/usr/bin/python

import sys, io, os, time, struct
from subprocess import call

outfilename = "raw.520s.txt"
with open(outfilename, "w") as outfile:
    for n in range(520):
        outputname = "--stream-to=/mnt/ramdisk/rg10.raw." + str(n) 
        outputname2 = "/mnt/ramdisk/rg10.raw." + str(n) 
        call(["v4l2-ctl", "-d", "/dev/video0", "--set-fmt-video=width=1920,height=1079,pixelformat=RG10", "--set-ctrl", "bypass_mode=0", "--stream-mmap", "--stream-count=1", outputname]) 
        time.sleep(1)
        call(["xxd", "-c", "48", "-l", "48", outputname2], stdout=outfile)
        os.remove(outputname2)

