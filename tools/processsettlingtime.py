#!/usr/bin/python

import sys, io, os, time, struct, getopt
from subprocess import call

outfilename = "trysettle.txt"
threshhold = 8
try:
    opts, args = getopt.getopt(sys.argv[1:], "ht:f:", ["help", "threshhold=", "filename="])
except getopt.GetoptError:
    print('processsettlingtime.py -f <setting time log file>')
    sys.exit(2)

for opt, arg in opts:
    if opt == '-h':
        print('processsettlingtime.py -t <thresh hold value for correct frames> -f <setting time log file>')
    elif opt in ("-t", "--threshhold"):
    	threshhold = int(arg)
    elif opt in ("-f", "--filename"):
        outfilename = arg

with open(outfilename, "r") as outfile:
    linenum = 0
    setNum = 0
    lp = 0
    frameStatus = []
    settleInfo = ""
    clksInfo = { }
    for line in outfile:
        lp = linenum % 44
        if lp == 0:
            settleInfo = line.rstrip()
        if ( lp > 21) and (lp < 42):
            frameStatus.append(line)
        if len(frameStatus) == 20:
            correctNum = 0;
            for status in frameStatus:
                res = status.split(' ')
                if (len(res) > 2) and (res[2] == "[-]"):
                    correctNum +=1
            if correctNum >= threshhold:
                print(settleInfo + ":corrected:" + str(correctNum))
                clks = settleInfo.split(':')
                if clks[3] in clksInfo:
                    clksInfo[clks[3]] += 1
                else:
                    clksInfo[clks[3]] = 1
            frameStatus.clear()
        linenum += 1
    for key, value in clksInfo.items():
        print("clk:" + key + ":total:" + str(value))
    
            
