#include "fc2ctrl.h"
#include <sys/mman.h>

uint32_t* iomap(const char* str, uint32_t size)
{
    char buf[8192];
    int fd = open("/proc/iomem", O_RDONLY);
    if (fd < 0) return NULL;
    
    int sz = read(fd, buf, sizeof(buf));
    close(fd);
    
    if(sz <=0 || sz>= sizeof(buf))return NULL;
    buf[sz] = 0;
    
    char* p = strstr(buf, str);
    if(p == NULL) return NULL;
    while(p > buf && *p != '\n') {
        if(*p == '-')*p = 0;
        p--;
    }
    p++;
    uint32_t off = strtoul(p, NULL, 16);
    if(off == 0) return NULL;
    
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd < 0) return NULL;

	p = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, off);
    close(fd);

	if (p == MAP_FAILED) return NULL;
    return (uint32_t*)p;
}

void iounmap(uint32_t* p, uint32_t size)
{
    if(p) munmap(p, size);
}
