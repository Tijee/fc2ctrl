#!/usr/bin/python

# cd "AppData\Roaming\Notepad++\plugins\Config\NppFTP\Cache\pi@192.168.1.41\home\pi\fc2pi"

import sys, socket, cv2, zlib, time
import numpy as np

class MJPEG:
    def __init__(self, addr):
        self.cs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.cs.connect(addr)
        self.buf = bytearray()
        self.length = 0
        self.idx = 0
        self.readhdr()  # main header

    def recv(self, req):
        while req > len(self.buf):
            b = bytearray( self.cs.recv(65536) )
            if len(b) == 0: raise Exception("disconnect")
            self.buf += b
    
    def readhdr(self):
        while True:
            self.recv(len(self.buf) + 8)
            i = 0
            while i+3 < len(self.buf):
                if self.buf[i] == 13 and self.buf[i+1] == 10 and self.buf[i+2] == 13 and self.buf[i+3] == 10:
                    break
                i += 1
            if i+3 >= len(self.buf): continue
            b = self.buf[:i+4].decode("utf8")
            self.buf = self.buf[i+4:]
            for t in b.split('\r\n'):
                ts = t.split(': ')
                if len(ts) == 2 and ts[0] == 'Content-Length':
                    self.length = int(ts[1])
                    print 'frame[%d] length=%d' % (self.idx, self.length)
                    self.idx += 1
            return
        
    def read(self):
        self.readhdr()  # multipart header
        self.recv(self.length)
        b = self.buf[:self.length]
        self.buf = self.buf[self.length:]
        return b


src = None
while(True):
    addr = ''
    try:
        if src == None:
            host = '192.168.1.41'
            if len(sys.argv) > 1 and '192.' in sys.argv[1]: host = sys.argv[1]
            addr = (host, 4000)
            src = MJPEG(addr)

        img = src.read()
        if '-save' in sys.argv:
            with open('%s_%d.jpg' % (sys.argv[-1], src.idx), 'wb') as f:
                f.write(img)
                
        frame = cv2.imdecode(np.asarray(img, dtype=np.uint8), cv2.IMREAD_COLOR)
        if '-half' in sys.argv:
            h, w, d = frame.shape
            frame = cv2.resize(frame, (w/2, h/2))
    except:
        print 'connect %s ...' % str(addr)
        time.sleep(1)
        del src
        src = None
        continue
        
    # Display the resulting frame
    cv2.imshow('mjpeg', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
        
cv2.destroyAllWindows()
