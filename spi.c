#include "fc2ctrl.h"
#include <linux/spi/spidev.h>
#include <sys/mman.h>

#ifdef PLATFORM_PI
static char spidevice[] = "/dev/spidev0.0";
static int spitimeout = 30000;
#else
static char spidevice[] = "/dev/spidev3.0";
static int spitimeout = 10000;
#endif
static int spispeed = 15*1000*1000;
static uint32_t spiret[1024];
static int spircnt = 0;
int spiverbose = 0;

int spictrl(const uint32_t* cmds, int cnt, int maxrcnt, const uint32_t* modify)
{
    static int fd = -1;
    static uint8_t tx[140] = {0x4b, 0xFF, 0xFF, 0xFF};
    static uint8_t rx[140];

    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long)tx,
        .rx_buf = (unsigned long)rx,
        .len = 4,
        .delay_usecs = 5,
        .speed_hz = spispeed,
        .bits_per_word = 8,
    };
    int ret, i, rcnt, chnl, icnt, wait;
    uint32_t cptr;
    char smsg[64];
    strcpy(smsg, "-");
    
    if(fd<0){
        fd = open(spidevice, O_RDWR);
        if(fd < 0)return 1;
        
        i = SPI_MODE_3;
        ret = ioctl(fd, SPI_IOC_WR_MODE, &i);
        if(ret < 0){ close(fd); fd = -1; return 2; }
        i = 8;
        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &i);
        if(ret < 0){ close(fd); fd = -1; return 3; }
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &spispeed);
        if(ret < 0){ close(fd); fd = -1; return 4; }
        
        for(i = 0; i < 3; i++){
            ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
            if(ret < 1) { close(fd); fd = -1; return 5; }
            if(rx[1] == 0x31 || rx[2] == 0x32 || rx[3] == 0x33) break;
        }
        if(i == 3) { close(fd); fd = -1; return 6; }
    }
    
    if(cnt == 1){ // memory read
        spircnt = 0;
        if(maxrcnt > 1024) maxrcnt = 1024;
        if(maxrcnt <= 0) return 0;
        
        unsigned addr = cmds[0];
        while(spircnt < maxrcnt){
            tx[0] = 0x0c;
            tx[1] = addr>>24;
            tx[2] = addr>>16;
            tx[3] = addr>>8;
            tx[4] = addr>>0;
            addr += 0x80;
            tr.len = 134;
            ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
            if(ret < 1) { close(fd); fd = -1; return 5; }
            for(int i=6; i<6+32*4; i+=4){
                spiret[spircnt] = rx[i] + (rx[i+1]<<8) + (rx[i+2]<<16) + (rx[i+3]<<24);
                spircnt++;
            }
        }
        return 0;
    }
    
    
    cptr = 0;
    rcnt = 0;
    icnt = 0;
    wait = 0;
    while(1){
        int c = wait < 100 ? 0 : cnt > 24 ? 24 : cnt;
        
        tx[0] = 0x05; // status
        tr.len = 5;
        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
        if(ret < 1) { close(fd); fd = -1; return 7; }
        if(rx[0] & 4){  // input fifo almost full
            c = 0;
            icnt += 1;
            if(icnt == spitimeout)return 8; // pi3 speed
            if(spiverbose == 1){
                printf("IF full %d\n", icnt);
            }
        } else {
            if(cnt) icnt = 0;
            
            // flash erasing friendly: wait after read buffer command.
            for(i=0; i<c; i++){
                if((cmds[cptr+i]>>16) == 0xfc00){  
                    c = i + 1;
                    wait = 98;
                    break;
                }
            }
        }
        if((rx[0] & 8) == 0) { // output fifo empty
            wait += 1;
            if(cnt == 0){
                icnt += 1;
                if(icnt == spitimeout/20)break;
            }
        }
        if(c == 0){
            tx[0] = 0x22; // read
            tr.len = 1 + 24*4;
            for(i=1; i<sizeof(tx); i++)tx[i] = 0;
            
        } else {
            tx[0] = 0x02; // write
            tr.len = 1 + c*4;
            cnt -= c;
            for(i=0; i<c; i++){
                uint32_t v = cmds[cptr];
                if(modify){
                    if(modify[0] == 0) modify = NULL;
                    else if(modify[0] <= cptr) {
                        if(modify[0] == cptr)v = modify[1];
                        modify += 2;
                    }
                }
                cptr += 1;
                tx[i*4+1] = v;
                tx[i*4+2] = v>>8;
                tx[i*4+3] = v>>16;
                tx[i*4+4] = v>>24;
            }
        }
        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
        if(ret < 1) { close(fd); fd = -1; return 7; }
        
        if(maxrcnt < 0){
            if(cnt == 0) return 0;
            continue;
        }
        
        
        if(spiverbose == 1){
            int nz = 0;
            for(i=1; i<tr.len; i++)if(rx[i]) nz = 1;
            if(nz){
                for(i=0; i<tr.len; i++){
                    printf("%02x ", rx[i]);
                }
                printf("\n");
            }
        }
        
        
        i = 1;
        while(i < tr.len){
            if(spircnt >= maxrcnt && rcnt == 0){
                if(chnl == 15 && wait>=90 && !maxrcnt){
                    if(spircnt == 1 && (spiret[0]>>16) == 0x5555){
                        spiret[0] &= 0xffff;
                        if(spiret[0] <= 100){
                            if(spiverbose == 2){
                                printf("%s %02d%%\n", smsg, spiret[0]);
                            } else {
                                printf("    \r%s %02d%%", smsg, spiret[0]);
                            }
                        } else if((spiret[0] & 0xff00) == 0x100){
                            switch(spiret[0] & 0xff){
                            case 0: strcpy(smsg, "Erasing"); break;
                            case 1: strcpy(smsg, "Programming"); break;
                            case 2: strcpy(smsg, "Verifying"); break;
                            case 3: strcpy(smsg, "Testing"); break;
                            default: sprintf(smsg, "ST[%d]", spiret[0]&0xff); break;
                            }
                            if(spiverbose != 2)printf("   \r%s ", smsg);
                        } else if(spiret[0] >= 0xf000){
                            printf("\n%s ERROR=%d\n", smsg, spiret[0]&0xfff);
                            cnt = 0; // error. disable output
                        } else {
                            printf("\n%s CODE=0x%04x\n", smsg, spiret[0]);
                        }
                        fflush(stdout);
                    } else if(spircnt > 0){
                        printf("output[%d]: ", spircnt);
                        for(rcnt = 0; rcnt < spircnt; rcnt ++){
                            printf("%08x ", spiret[rcnt]);
                        }
                        printf("\n");
                    }
                }
                rcnt = 0;
                if(!maxrcnt)spircnt = 0;
            }
            while(rcnt == 0 && i < tr.len){
                if(rx[i+3] == 0x50){
                    rcnt = rx[i] + rx[i+1]*256;
                    rcnt = rcnt/4+1;
                    chnl = rx[i+2]&0xf;
                }
                i += 4;
            }
            while(rcnt > 0){
                if(i >= tr.len)break;
                if(maxrcnt == 0 || chnl == 15){
                    spiret[spircnt] = rx[i] + (rx[i+1]<<8) + (rx[i+2]<<16) + (rx[i+3]<<24);
                    spircnt++;
                }
                rcnt--;
                i += 4;
            }
        }
    }
    return 0;
}


int spirun(const char* fname, const uint32_t* modify) {
    struct stat sb;
    uint32_t* cmds;
    int cnt;
    int fd = open(fname, O_RDONLY);
    if(fd < 0){
        perror("open");
        return 1;
    }
    if(fstat(fd, &sb) == -1){
        perror("fstat");
        return 2;
    }
    if(sb.st_size < 8 || sb.st_size % 4 != 0) {
        fprintf(stderr, "bad bin file size %s\n", fname);
        return 3;
    }
    cnt = sb.st_size / 4;
    cmds = (uint32_t*)mmap (0, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if(cmds == NULL){
        perror ("mmap");
        return 4;
    }
    if(cmds[cnt-1] != 0){
        fprintf(stderr, "bad bin file footer %s\n", fname);
        return 5;
    }
    close(fd);
    if((cmds[0] & 0xff000000) != 0xfc000000){
        const char* msg = (const char*)cmds;
        cmds += 16;
        cnt -= 16;
        if((cmds[0] & 0xff000000) != 0xfc000000){
            fprintf(stderr, "bad bin file header %s\n", fname);
            return 5;
        }
        printf("INFO: %s\n", msg);
        if(strstr(msg, " flash ")) spitimeout *= 4;
    }
    printf("RET=%d\n", spictrl(cmds, cnt, 0, modify));
    munmap(cmds, sb.st_size);
    return 0;
}

const uint32_t* getoutinfo(int chnl, int* type)
{
    if(!spircnt){
        int retry=0;
        for(; retry<3; retry++){
            unsigned addr = 0xf0000;
            int ret = spictrl(&addr, 1, 1024, NULL);
            if(ret != 0){
                fprintf(stderr, "E: spictrl return=%d\n", ret);
                return NULL;
            }
            if(spiret[0] == 0x49440001)break;
        }
        if(spircnt != 1024){
            fprintf(stderr, "E: spictrl return data size = %d\n", spircnt);
            return NULL;
        }
        if(retry == 3){
            spircnt = 0;
            fprintf(stderr, "E: incorrect outinfo header\n");
            return NULL;
        }
    }
    
    const uint32_t * p = spiret;
    const uint32_t * plast = NULL;
    p += 1;
    while(*p){
        if((*p>>24) != 0xc0){
            fprintf(stderr, "E: incorrect outinfo section\n");
            return NULL;
        }
        if(((*p>>16) & 0xf) != chnl) {
            if((*p & 0xffff) > 0 && ((*p>>20)&0xf) == 1) plast = p+1;  // jpeg use empty data to indicate last data is ok
            p += (*p & 0xffff) + 1;
            if(p >= spiret+1024){
                fprintf(stderr, "E: incorrect outinfo data\n");
                return NULL;
            }
            continue;
        }
        *type = (*p>>20)&0xf;
        if((*p & 0xffff) > 0) return p+1;
        return plast;
    }
    return NULL;
}

int readmem(uint32_t addr) {
    int ret;
    if(addr < 0x40000){
        static uint32_t rmem[] = {
            0xfc610800, 0xfc600000,
            0xfc100000, 0xfc000000,
            0, 0
        };
        rmem[0] |= addr>>18;
        rmem[1] |= (addr>>2)&0xffff;
        ret = spictrl(rmem, sizeof(rmem)/4, 64, NULL);
    } else {
        ret = spictrl(&addr, 1, 64, NULL);
    }
    if(ret)return ret;

    addr = addr - (addr&3);
    for(int i=0; i<8; i++){
        printf("0x%06x: ", addr + i*32);
        for(int j=0; j<8; j++){
            printf("%08x ", spiret[i*8+j]);
        }
        printf("\n");
    }
    return 0;
}

int writemem(const char* saddr, const char* sdata) {
    uint32_t addr = strtoul(saddr, NULL, 16);
    uint32_t step = 0x1000;
    uint32_t acnt = 1;
    uint32_t data = strtoul(sdata, NULL, 16);
    uint32_t dcnt = 16;

    const char* s = strchr(saddr, '+');
    if(s) step = strtoul(s+1, NULL, 16);
    s = strchr(saddr, '*');
    if(s) acnt = strtoul(s+1, NULL, 10); 
    s = strchr(sdata, '*');
    if(s) dcnt = strtoul(s+1, NULL, 10); 

    printf("[0x%08x] * %d -> @0x%08x + 0x%04x * %d\n", data, dcnt, addr, step, acnt);
    if(dcnt < 1 || dcnt > 64){
        printf("E: invalid dcnt\n");
        return 1;
    }
    if(acnt < 1 || acnt > 10000){
        printf("E: invalid acnt\n");
        return 1;
    }

    uint32_t* cmds = (uint32_t*)malloc((60+dcnt+3*acnt)*4);
    if(cmds == NULL) {
        printf("E: malloc() return NULL\n");
        return 2;
    }
    uint32_t* c = cmds;
    *c++ = 0xfc080000 | (dcnt&0x3f);
    while(c < cmds+dcnt+1){
        *c++ = (uint32_t)data;
    }
    while(acnt>0){
        *c++ = 0xfc610800 | (addr>>18);
        *c++ = 0xfc600000 | ((addr>>2)&0xffff);
        *c++ = 0xfc10f000 | (dcnt&0x3f);
        addr += step;
        acnt -= 1;
    }
    *c++ = 0;
    *c++ = 0;
    int ret = spictrl(cmds, c-cmds, 0, NULL);
    free(cmds);
    return ret;
}

