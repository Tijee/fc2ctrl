#ifndef  GST_H
#define  GST_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <gst/audio/audio.h>
#include "csi.h"

int pipeline_stop();
int audiopipeline_init(RXCHNL* chnl);
int videopipeline_init(RXCHNL* chnl);


#endif
