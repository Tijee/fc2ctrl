all: fc2ctrl fc2gpio.so

TEGRA:=$(shell cat /sys/module/tegra_fuse/parameters/tegra_chip_id)

CFLAGSGST=`pkg-config --cflags gstreamer-1.0`
LFLAGSGST=`pkg-config --libs gstreamer-1.0`
ifeq ($(TEGRA),)

# apt install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev

PLATFORM=PLATFORM_PI
GPIOC=gpiopi.c
CSIC=csipi.c
CFLAGS=-std=gnu99 -D$(PLATFORM) -Wno-unused-result -I/opt/vc/include/ -I/opt/vc/include/interface/vctypes/ -L/opt/vc/lib/
LFLAGS=-lz -lmmal_core -lmmal_util -lmmal_vc_client -lvcos -lbcm_host /usr/lib/arm-linux-gnueabihf/libjpeg.a

else

ifeq ($(TEGRA),64)
PLATFORM=PLATFORM_TK1
endif

ifeq ($(TEGRA),33)
PLATFORM=PLATFORM_TX1
endif

ifeq ($(TEGRA),24)
PLATFORM=PLATFORM_TX2
endif

GPIOC=gpiotegra.c
CSIC=csitegra.c
#CFLAGS=-std=gnu99 -D$(PLATFORM) -DSDECODE_ERR -Wno-unused-result -Iinclude
CFLAGS=-std=gnu99 -D$(PLATFORM) -Wno-unused-result -Iinclude
LFLAGS=-lz /usr/lib/aarch64-linux-gnu/libjpeg.a


endif

FILEC=fc2ctrl.c funcs.c spi.c gst.c $(GPIOC) $(CSIC)
FILEH=gpio.h csi.h queue.h spi.h sdecode.h fc2ctrl.h gst.h


fc2gpio.so: funcs.c $(GPIOC) $(FILEH) $(GCC) 
	gcc -O3 $(CFLAGS) -shared -o fc2gpio.so -fPIC funcs.c $(GPIOC)
    
fc2ctrl: $(FILEC) $(FILEH) $(GCC) 
	gcc -O3 $(CFLAGS) -pthread `pkg-config --cflags gstreamer-1.0 gstreamer-app-1.0 gstreamer-audio-1.0` -o fc2ctrl $(FILEC) $(LFLAGS) `pkg-config --libs gstreamer-1.0 gstreamer-app-1.0 gstreamer-audio-1.0`

dbg: $(FILEC) $(FILEH) $(GCC) 
	gcc -fno-omit-frame-pointer -funwind-tables -g $(CFLAGS) `pkg-config --cflags gstreamer-1.0 gstreamer-app-1.0 gstreamer-audio-1.0` -o fc2ctrl $(FILEC) $(LFLAGS) `pkg-config --libs gstreamer-1.0 gstreamer-app-1.0 gstreamer-audio-1.0`
    
test: fc2ctrl
	sudo ./fc2ctrl -t

clean:
	-rm fc2ctrl fc2gpio.so
