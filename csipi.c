#include "fc2ctrl.h"
#include "interface/vcos/vcos.h"
#include "bcm_host.h"

#include "interface/mmal/mmal.h"
#include "interface/mmal/mmal_buffer.h"
#include "interface/mmal/mmal_logging.h"
#include "interface/mmal/util/mmal_default_components.h"
#include "interface/mmal/util/mmal_util.h"
#include "interface/mmal/util/mmal_util_params.h"
#include "interface/mmal/util/mmal_connection.h"
#include "csi.h"


static MMAL_POOL_T* vpool;
static MMAL_PORT_T* vinput;

static void icallback(MMAL_PORT_T *port, MMAL_BUFFER_HEADER_T *buffer)
{
    mmal_buffer_header_release(buffer);
}

static void callback(MMAL_PORT_T *port, MMAL_BUFFER_HEADER_T *buffer)
{
    static int setpriority = 0;
    if(setpriority == 0){
        struct sched_param params;
        pthread_t this_thread = pthread_self();
        params.sched_priority = sched_get_priority_max(SCHED_FIFO);
        pthread_setschedparam(this_thread, SCHED_FIFO, &params);
        setpriority = 1;
    }
    
    if(vsize == 0){
        mmal_buffer_header_release(buffer);
        return;
    }
    
    RXCHNL* chnl = NULL;
    if(chnls[15])chnl = (RXCHNL*)(chnls[15]->ext);

    
    if( buffer->length >= vsize ){
        if(vtype != 15 && !(buffer->flags&0x80)) {  // RAW TYPE
            MMAL_BUFFER_HEADER_T* obuf = NULL;
            if(chnl && chnl->fd < 0 && vinput)obuf = mmal_queue_get(vpool->queue);
            
            for(int k=0; k<vsize; k+=vline){
                if(chnl) chnl->dcnt += vline/4;
                uint8_t* mem = &buffer->data[k];
                int y = k/vline;
                
                if(chnl && chnl->recv)(*chnl->recv)(chnl, (uint32_t*)mem, dline, y);
                
                int w = dline;
                if(w > 1920*2)w = 1920*2;
                if(obuf && y%4 == 0 && y < 1080*4){
                    uint8_t* mem1 = &buffer->data[k+vline];
                    uint8_t* line = &obuf->data[1920/4*3*y];
                    for(int x=0; x<w; x+=4){
                        *line++ = mem[x];
                        *line++ = mem[x+1];
                        *line++ = mem1[x+1];
                    }
                }
            }
            if(obuf) {
                obuf->length = 1920*1080*3;
                mmal_port_send_buffer(vinput, obuf);
            }
        } else if(vtype == 15 && (buffer->flags&0x80)) { // INVALID DATA
            
        } else {  // STREAM TYPE
            if(chnl && chnl->fd>0){
                write(chnl->fd, buffer->data, buffer->length);
            }
            
            sdecode(buffer->data, dline, vsize);

            //if(chnls[14]){
            //    chnl = (RXCHNL*)(chnls[14]->ext);
            //    chnl->vars[0] += 1;
            //}
        }
    }
    
    buffer->length = 0;
    mmal_port_send_buffer(port, buffer);
    framecnt ++;
}


OBUF* obufget()
{
    if(!vinput || !vpool)return NULL;
    return (OBUF*)mmal_queue_get(vpool->queue);
}

void obufsend(OBUF* obuf)
{
    mmal_port_send_buffer(vinput, (MMAL_BUFFER_HEADER_T*)obuf);
}

void obufrelease(OBUF *obuf)
{
    mmal_buffer_header_release((MMAL_BUFFER_HEADER_T*)obuf);
}


void csisetsize(int w, int h)
{
    // rawcam stores RAW8 data in RAW10 buffer
    vline = w*5/4;
    if(vline % 32) vline += 32 - vline%32;
    dline = vtype == 8 ? w : vline;
    vsize = vline * h;
}


uint32_t rxtiming[9] = {1000};
int csirun(int w, int h)
{
    MMAL_COMPONENT_T *rawcam = NULL;
    MMAL_PORT_T *output = NULL;
    MMAL_POOL_T *pool = NULL;
    MMAL_STATUS_T status;

    MMAL_COMPONENT_T *video = NULL;
    MMAL_POOL_T* vcpool = NULL;
    
    vinput = NULL;
    vpool = NULL;

    
    MMAL_PARAMETER_CAMERA_RX_CONFIG_T rx_cfg = {{MMAL_PARAMETER_CAMERA_RX_CONFIG, sizeof(rx_cfg)}};
    
    bcm_host_init();

    status = mmal_component_create("vc.ril.rawcam", &rawcam);
    if(status != 0) {
        fprintf(stderr, "E: create vc.ril.rawcam return %d\n", status);
        goto runend;
    }
    output = rawcam->output[0];
    status = mmal_port_parameter_get(output, &rx_cfg.hdr);
    rx_cfg.unpack = 0;
    rx_cfg.pack = 0;
    rx_cfg.data_lanes = 2;
    rx_cfg.image_id = vtype == 15 ? 0x30 : vtype == 8 ? 0x2a : 0x2b;  // stream/raw8/raw10
    status = mmal_port_parameter_set(output, &rx_cfg.hdr);
    if(status != 0) {
        fprintf(stderr, "E: mmal_port_parameter_set return %d\n", status);
        goto runend;
    }
    
    MMAL_PARAMETER_CAMERA_RX_TIMING_T rx_timing = {{MMAL_PARAMETER_CAMERA_RX_TIMING, sizeof(rx_timing)}};
    mmal_port_parameter_get(output, &rx_timing.hdr);
    uint32_t* ptiming = &rx_timing.timing1;
    fprintf(stderr, "timing=");
    for(int i=0; i<9; i++){
        if(rxtiming[i] < 100) ptiming[i] = rxtiming[i];
        fprintf(stderr, "%u,", ptiming[i]);
    }
    fprintf(stderr, "\n");
    mmal_port_parameter_set(output, &rx_timing.hdr);
    
    int viewid = -1;
    for(int i=0; i<16; i++)if(chnls[i]){
        RXCHNL* chnl = (RXCHNL*)(chnls[i]->ext);
        if(!chnl->info || chnl->fd>0)continue;
        viewid = i;
        break;
    }
    
    if(viewid > 0){
        status = mmal_component_create(MMAL_COMPONENT_DEFAULT_VIDEO_RENDERER, &video);
        if(status != 0) {
            fprintf(stderr, "E: create VIDEO_RENDERER return %d\n", status);
            goto runend;
        }
        vcpool = mmal_port_pool_create(video->control, video->control->buffer_num, video->control->buffer_size);
        mmal_port_enable(video->control, icallback);
        
        // RAW -> VIDEO
        vinput = video->input[0];
        
        vinput->format->es->video.crop.width = 1920;
        vinput->format->es->video.crop.height = 1080;
        vinput->format->es->video.width = VCOS_ALIGN_UP(1920, 16);
        vinput->format->es->video.height = VCOS_ALIGN_UP(1080, 16);
        vinput->format->encoding = MMAL_ENCODING_RGB24;
        mmal_port_format_commit(vinput);

        vpool = mmal_port_pool_create(vinput, vinput->buffer_num, vinput->buffer_size);
        vinput->userdata = (struct MMAL_PORT_USERDATA_T *)vpool;
        status = mmal_port_enable(vinput, icallback);
        if(status != 0) {
            fprintf(stderr, "E: mmal_port_enable input return %d\n", status);
            goto runend;
        }
    }
    
    status = mmal_component_enable(rawcam);
    if(status != 0) {
        fprintf(stderr, "E: mmal_component_enable return %d\n", status);
        goto runend;
    }
    
    output->format->es->video.crop.width = w;
    output->format->es->video.crop.height = h+16;
    output->format->es->video.width = w;
    output->format->es->video.height = h+16;
    output->format->encoding = MMAL_ENCODING_BAYER_SGBRG10P;

    status = mmal_port_format_commit(output);
    if(status != 0) {
        fprintf(stderr, "E: mmal_port_format_commit return %d\n", status);
        goto runend;
    }
    
    output->buffer_size = output->buffer_size_recommended;
    if(output->buffer_size < vsize) output->buffer_size = vsize;
    output->buffer_num = output->buffer_num_recommended;
    if(output->buffer_num < 10) output->buffer_num = 10;
    
    status = mmal_port_parameter_set_boolean(output, MMAL_PARAMETER_ZERO_COPY, MMAL_TRUE);
    
    pool = mmal_port_pool_create(output, output->buffer_num, output->buffer_size);
    
    if(!pool){
        fprintf(stderr, "E: mmal_port_pool_create return NULL\n");
        goto runend;
    }
    
    output->userdata = (struct MMAL_PORT_USERDATA_T *)output;
    status = mmal_port_enable(output, callback);
    if(status != 0){
        fprintf(stderr, "E: mmal_port_enable return %d\n", status);
        goto runend;
    }
    for(int i = 0; i<output->buffer_num; i++)
    {
        MMAL_BUFFER_HEADER_T *buffer = mmal_queue_get(pool->queue);
        memset(buffer->data, 0, output->buffer_size);
        mmal_port_send_buffer(output, buffer);
    }
    
    int cc = 0;
    struct timespec spec;
    clock_gettime(CLOCK_MONOTONIC, &spec);
    long ms = spec.tv_sec * 1000 + spec.tv_nsec / 1000000;
    while(seconds>0){
        fprintf(stderr, "[%d] ", cc++);
        for(int i=0; i<16; i++)if(chnls[i]){
            RXCHNL* chnl = (RXCHNL*)(chnls[i]->ext);
            int c = chnl->dcnt - chnl->odcnt;
            chnl->odcnt += c;
            
            if(c){
                if(chnl->type & 0x10)c = c/(vsize/400);
                fprintf(stderr, "%d=%d ", i, c);
            }
        }
        if(scntbytes>10000){
            fprintf(stderr, "BER=%d", (scntebits*1000)/(scntbytes/1000));
            scntbytes = 0;
            scntebits = 0;
        }
        fprintf(stderr, "\n");
        
        while(1){
            clock_gettime(CLOCK_MONOTONIC, &spec);
            unsigned m = (unsigned)(spec.tv_sec * 1000 + spec.tv_nsec / 1000000 - ms);
            if( m >= 1000) break;
            
            vcos_sleep(50);
            for(int i=0; i<16; i++)if(chnls[i]){
                RXCHNL* chnl = (RXCHNL*)(chnls[i]->ext);
                if(chnl->run)(*chnl->run)(chnl);
                
                if(chnl->type == 1 && chnl->vars[1] == 1000){
                    chnl->vars[1] += 1;
                }
            }
        }
        seconds -= 1;
        ms += 1000;
    }
    
    
runend:
    vsize = 0;
    
    if(vinput) mmal_port_disable(vinput);
    if(vpool) mmal_port_pool_destroy(vinput, vpool);
    if(video){
        mmal_port_disable(video->control);
        mmal_port_pool_destroy(video->control, vcpool);
        mmal_component_disable(video);
    }
    
    if(output) mmal_port_disable(output);
    if(pool) mmal_port_pool_destroy(output, pool);
    if(rawcam) mmal_component_disable(rawcam);
    
    squit();
    return status;
}

