#ifndef _spi_h
#define _spi_h

int spictrl(const uint32_t* cmds, int cnt, int maxrcnt, const uint32_t* modify);
int spirun(const char* fname, const uint32_t* modify);

const uint32_t* getoutinfo(int chnl, int* type);
int readmem(uint32_t addr);
int writemem(const char* saddr, const char* sdata);


extern int spiverbose;

#endif
